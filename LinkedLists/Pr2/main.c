#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

/* Creating structure definition */
/* self - referential structure */
typedef struct node
{
    int data;
    struct node *nextPtr;
} node_t;

typedef node_t *ListNodePtr;
/* creating prototypes of function */
void insertNode(ListNodePtr *head, int value, int position);
void insertAtEnd(ListNodePtr *head, int value);
void insertAtBeg(ListNodePtr *head, int value);
int deleteNode(ListNodePtr *head, int position);
int isEmpty(ListNodePtr head);
void printList(ListNodePtr currentPtr);
int menu();
int updateNode(ListNodePtr *head, int newValue, int position);
int searchElem(ListNodePtr *head, int value);

int main()
{
    /* creating head pointer of the list */
    ListNodePtr head = NULL;
    char choice = 0; /* user's choice */
    int item = 0; /* int enter by user */
    int position = 0;
    /* displaying menu */

    while ((choice = menu()) != 8)
    {
        switch (choice)
        {
            case 1:
                printf("Specify integer to insert at the beggining of the list.\n");
                scanf("%d", &item);
                insertAtBeg(&head, item);
                break;
            case 2:
                printf("Specify integer to insert at the end of the list.\n");
                scanf("%d", &item);
                insertAtEnd(&head, item);
                break;
            case 3:
                printf("Specify integer to insert into chosen position.\n");
                scanf("%d", &item);
                printf("Enter position: ");
                scanf("%d", &position);
                insertNode(&head, item, position);
                break;
            case 4:
                if (!isEmpty(head))
                {
                    printf("Enter position of value to be delete:\n");
                    scanf("%d", &position);
                    if ((item = deleteNode(&head, position)))
                        printf("%d deleted.\n", item);
                    else
                        printf("Position %d not found.\n", position);
                }
                else
                {
                    printf("List is empty!");
                    printf(" Nothing to delete.\n");
                }
                break;
            case 5:
                printf("Specify node to update.\n");
                scanf("%d", &position);
                printf("Specify new value.\n");
                scanf("%d", &item);
                if (!isEmpty(head))
                    updateNode(&head, item, position);
                else
                {
                    printf("List is empty!");
                    printf(" Nothing to delete.\n");
                }
                break;
            case 6:
                printf("Enter value to search for: ");
                scanf("%d", &item);
                if (!isEmpty(head))
                    if ((position = searchElem(&head, item)))
                        printf("Element %d found at %d position.\n", item, position);
                    else
                        printf("Element %d not found.\n", item);
                else
                    printf("List is empty!\n");
                break;
            case 7:
                printList(head);
                break;
            default:
                printf("Wrong input. Specify input from 1 to 8.\n");
                break;
        }
    }
    printf("end of program.\n");
    return 0;
}
/* Definition of function on linked list */
void insertAtBeg(ListNodePtr *head, int value)
{
    ListNodePtr new_node = malloc(sizeof(node_t));
    new_node->data = value;
    new_node->nextPtr = *head;
    *head = new_node;
}

void insertAtEnd(ListNodePtr *head, int value)
{
    ListNodePtr current = *head;

    if (current != NULL)
    {
        while (current->nextPtr != NULL)
            current = current->nextPtr; //przesuwa wskaznik na koniec

        /* Now we can add new variable */
        current->nextPtr = malloc(sizeof(node_t));
        current->nextPtr->data = value;
        current->nextPtr->nextPtr = NULL;
    }
    else
    {
        current = malloc(sizeof(node_t));
        current->data = value;
        current->nextPtr = NULL;
        *head = current;
    }
}
void insertNode(ListNodePtr *head, int value, int position)
{
    ListNodePtr newPtr; //pointer to new node
    ListNodePtr previousPtr; //pointer to previous node in list
    ListNodePtr currentPtr; //pointer to current node in list

    newPtr = malloc(sizeof(node_t));

    if (newPtr != NULL)
    {
        int i = 0;
        position -= 1;
        newPtr->data = value;
        newPtr->nextPtr = NULL;

        previousPtr = NULL;
        currentPtr = *head;

        while (currentPtr != NULL && i != position)
        {
            previousPtr = currentPtr;
            currentPtr = currentPtr->nextPtr;
            i++;
        }

        if (previousPtr == NULL)
        {
            newPtr->nextPtr = *head;
            *head = newPtr;
        }
        else
        {
            previousPtr->nextPtr = newPtr;
            newPtr->nextPtr = currentPtr;
        }
    }
    else
    {
        printf("%d not inserted. No memory available.\n", value);
    }
}

int deleteNode(ListNodePtr *head, int position)
{
    int i = 1;
    
    int deletedVal = 0;
    ListNodePtr previousPtr;
    ListNodePtr currentPtr;
    ListNodePtr tempPtr = NULL;

    if (position == 1)
    {
        tempPtr = *head;
        *head = (*head)->nextPtr;
        deletedVal = tempPtr->data;
        free(tempPtr);
        return deletedVal;
    }
    else
    {
        previousPtr = *head;
        currentPtr = (*head)->nextPtr;

        while (currentPtr != NULL && i != (position - 1))
        {
            previousPtr = currentPtr;
            currentPtr = currentPtr->nextPtr;
            i++;
        }

        if (currentPtr != NULL)
        {
            tempPtr = currentPtr;
            previousPtr->nextPtr = currentPtr->nextPtr;
            deletedVal = tempPtr->data;
            free(tempPtr);
            return deletedVal;
        }
    }

    return 0;
}

int updateNode(ListNodePtr *head, int newValue, int position)
{
    int i = 1;
    ListNodePtr currentPtr;

    currentPtr = *head;

    while (currentPtr != NULL && i != position)
    {
        currentPtr = currentPtr->nextPtr;
        i++;
    }

    if (currentPtr != NULL)
    {
        currentPtr->data = newValue;
        printf("Updated succesfully.\n");
        return position;
    }
    else
        printf("Value not found in list.\n");
    
    return 0;
}


int isEmpty(ListNodePtr head)
{
    return head == NULL;
}

void printList(ListNodePtr currentPtr)
{
    if (currentPtr == NULL)
        printf("List is empty.\n");
    else
    {
        printf("The list:\n");

        while (currentPtr)
        {
            printf("%d --> ", currentPtr->data);
            currentPtr = currentPtr->nextPtr;
        }
        printf("NULL\n");
    }
}

int searchElem(ListNodePtr *head, int value)
{
    int i = 1;
    ListNodePtr currentPtr = *head;
    int position = 0;

    while (currentPtr != NULL && currentPtr->data != value)
    {
        currentPtr = currentPtr->nextPtr;
        i++;
    }

    if (currentPtr != NULL)
        return position = i;
    
    return 0;
}

int menu()
{
    int ans;

    printf("Enter your choice:\n"
            " 1 to insert node at beggining.\n"
            " 2 to insert node at end.\n"
            " 3 to insert at a specific position.\n"
            " 4 to delete node from any position.\n"
            " 5 to update node value.\n"
            " 6 to search element in the linked list.\n"
            " 7 to display list.\n"
            " 8 to exit.\n");
    printf(":: ");
    scanf("%d", &ans);
    while ( (ans < 1 || ans > 8) && ans != 8)
    {
        printf("Possible choice from 1 to 7 and 8 to end program.\n");
        scanf("%d", &ans);
    }

    return ans;
}
