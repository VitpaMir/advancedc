#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

/* Creating structure definition */
/* self - referential structure */
typedef struct node
{
    char data;
    struct node *nextPtr;
} node_t;

typedef node_t *ListNodePtr;
/* creating prototypes of function */
void insertNode(ListNodePtr *head, char value);
void insertAtEnd(ListNodePtr *head, char value);
void insertAtBeg(ListNodePtr *head, char value);
char deleteNode(ListNodePtr *head, char value);
void deleteAtBeg(ListNodePtr *head);
int isEmpty(ListNodePtr head);
void printList(ListNodePtr currentPtr);
int menu();

int main()
{
    /* creating head pointer of the list */
    ListNodePtr head = NULL;
    char choice = '\0'; /* user's choice */
    char item = '\0'; /* char enter by user */
    /* displaying menu */

    while ((choice = menu()) != '6')
    {
        switch (choice)
        {
            case '1':
                printf("Specify character to insert into the list in alphabetical order.\n");
                scanf("%c", &item);
                while (getchar() != '\n')
                    continue;
                insertNode(&head, item);
                printList(head);
                break;

            case '2':
                printf("Specify character to insert at the end of the list.\n");
                scanf("%c", &item);
                insertAtEnd(&head, item);
                printList(head);
                break;

            case '3':
                printf("Specify character to insert at the end of the list.\n");
                scanf("%c", &item);
                insertAtBeg(&head, item);
                printList(head);
                break;

            case '4':
                if (!isEmpty(head))
                {
                    printf("Specify character to delete at the end of the list.\n");
                    scanf("%c", &item);
                    if (deleteNode(&head, item))
                    {
                        printf("%c deleted.\n", item);
                        printList(head);
                    }
                    else
                        printf("%c not found.\n", item);
                }
                else
                {
                    printf("List is empty!");
                    printf(" Nothing to delete.\n");
                }
                break;

            case '5':
                if (!isEmpty(head))
                {
                    deleteAtBeg(&head);
                    printf("%c deleted.\n", item);
                    printList(head);
                }
                else
                {
                    printf("List is empty!");
                    printf(" Nothing to delete.\n");
                }
                break;

            default:
                printf("Wrong input. Specify input from 1 to 6.\n");
                break;
        }
    }
    printf("end of program.\n");
    return 0;
}
/* Definition of function on linked list */
void insertAtBeg(ListNodePtr *head, char value)
{
    ListNodePtr new_node = malloc(sizeof(node_t));
    new_node->data = value;
    new_node->nextPtr = *head;
    *head = new_node;
}

void insertAtEnd(ListNodePtr *head, char value)
{
    ListNodePtr current = *head;

    if (current != NULL)
    {
        while (current->nextPtr != NULL)
            current = current->nextPtr; //przesuwa wskaznik na koniec

        /* Now we can add new variable */
        current->nextPtr = malloc(sizeof(node_t));
        current->nextPtr->data = value;
        current->nextPtr->nextPtr = NULL;
    }
    else
    {
        current = malloc(sizeof(node_t));
        current->data = value;
        current->nextPtr = NULL;
        *head = current;
    }
}
void insertNode(ListNodePtr *head, char value)
{
    ListNodePtr newPtr; //pointer to new node
    ListNodePtr previousPtr; //pointer to previous node in list
    ListNodePtr currentPtr; //pointer to current node in list

    newPtr = malloc(sizeof(node_t));

    if (newPtr != NULL)
    {
        newPtr->data = value;
        newPtr->nextPtr = NULL;

        previousPtr = NULL;
        currentPtr = *head;

        while (currentPtr != NULL && value > currentPtr->data)
        {
            previousPtr = currentPtr;
            currentPtr = currentPtr->nextPtr;
        }

        if (previousPtr == NULL)
        {
            newPtr->nextPtr = *head;
            *head = newPtr;
        }
        else
        {
            previousPtr->nextPtr = newPtr;
            newPtr->nextPtr = currentPtr;
        }
    }
    else
    {
        printf("%c not inserted. No memory available.\n", value);
    }
}
void deleteAtBeg(ListNodePtr *head)
{
    ListNodePtr tempPtr = NULL;

    if (head == NULL)
        return;
    else
    {
        tempPtr = *head;
        *head = (*head)->nextPtr;
        free(tempPtr);
    }
}

char deleteNode(ListNodePtr *head, char value)
{
    ListNodePtr previousPtr;
    ListNodePtr currentPtr;
    ListNodePtr tempPtr;

    if (value == (*head)->data)
    {
        tempPtr = *head;
        *head = (*head)->nextPtr;
        free(tempPtr);
        return value;
    }
    else
    {
        previousPtr = *head;
        currentPtr = (*head)->nextPtr;
    }

    while (currentPtr != NULL && currentPtr->data != value)
    {
        previousPtr = currentPtr;
        currentPtr = currentPtr->nextPtr;
    }

    if (currentPtr != NULL)
    {
        tempPtr = currentPtr;
        previousPtr->nextPtr = currentPtr->nextPtr;
        free(tempPtr);
        return value;
    }

    return '\0';
}

int isEmpty(ListNodePtr head)
{
    return head == NULL;
}

void printList(ListNodePtr currentPtr)
{
    if (currentPtr == NULL)
        printf("List is empty.\n");
    else
    {
        printf("The list:\n");

        while (currentPtr)
        {
            printf("%c --> ", currentPtr->data);
            currentPtr = currentPtr->nextPtr;
        }
        printf("NULL\n");
    }
}

int menu()
{
    int ans;

    printf("Enter your choice:\n"
            " 1 to insert an element into the list in alphabetical order.\n"
            " 2 to insert an element at the end of the list.\n"
            " 3 to insert an element at the beginning of the list.\n"
            " 4 to delete an element from the list.\n"
            " 5 to delete an element from the beginning of the list.\n"
            " 6 to end.\n");
    printf(":: ");

    ans = getchar();
    while (getchar() != '\n')
        continue;
    while ( (ans < '1' || ans > '6') && ans != '6')
    {
        printf("Possible choice from 1 to 5 and 6 to end program.\n");
        while (getchar() != '\n')
            continue;
    }
    return ans;
}
