#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    int mypid, myppid;
    mypid = getpid();
    myppid = getppid();
    printf("Process ID: %d\n", mypid);
    printf("Parent process ID: %d\n", myppid);
    printf("Cross verification of pid's executing process commands"
            "on shell\n");
    system("ps -ef");
}