/* Signals */
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <string.h>

int score = 0;

void end_game()
{
    printf("\nFinal score: %d\n", score);
    exit(0);
}

void error(char *msg)
{
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    exit(1);
}

static void hdl(int sig, siginfo_t *siginfo, void *context)
{
    if (sig == SIGALRM)
    {
        printf("\nTime is up!\n");
        end_game();
        exit(2);
    }
    else if (sig == SIGINT)
    {
        printf("\nKeyboard interrupt, program stopped manually.\n");
        end_game();
        exit(3);
    }
}

int main()
{
    struct sigaction act;
    act.sa_sigaction = &hdl;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

    srand(time(0));
    
    while(1)
    {
        int a = rand() % 11;
        int b = rand() % 11;

        char txt[4];
        

        printf("\nWhat is %d times %d: ", a, b);
        fgets(txt, 4, stdin);
        alarm(5);
        if (sigaction(SIGTERM, &act, NULL) < 0)
        {
            perror("sigaction"); //we have an error
            return 1;
        }
        else if (sigaction(SIGINT, &act, NULL) < 0)
        {
            perror("sigaction"); //we have an error
            return 1;
        }
        int answer = atoi(txt);

        if (answer == a * b)
            score++;
        else
            printf("\nWrong ! Score: %d\n", score);
    }

    return 0;
}