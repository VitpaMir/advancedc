#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    pid_t pid1, pid2;
    
    pid1 = fork();
    pid2 = fork();
    if (pid1 > 0 && pid2 > 0)
    {
        printf("Parent\n");
        printf("%d %d \n", pid1, pid2);
        printf("Parent process id: %d \n", getppid());
        printf("Process id: %d \n", getpid());
    }
    if (pid1 == 0 && pid2 > 0)
    {
        printf("First child\n");
        printf("%d %d \n", pid1, pid2);
        printf("(1)my id is %d\n", getpid());
        printf("(1)my parent id is %d\n", getppid());
    }
    if (pid1 > 0 && pid2 == 0)
    {
        printf("Second child\n");
        printf("%d %d \n", pid1, pid2);
        printf("(2)my id is %d\n", getpid());
        printf("(2)my parent id is %d\n", getppid());
    }
    else
    {
        printf("Third child\n");
        printf("%d %d \n", pid1, pid2);
        printf("(3)my id is %d\n", getpid());
        printf("(3)my parent id is %d\n", getppid());
    }

    return 0;
}