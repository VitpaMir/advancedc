#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#define MAX_COUNT 10

void childProcess();
void parentProcess();

int main()
{
    pid_t pid;
    puts("Poczatek programu.");
    pid = fork();

    if (pid == 0)
        childProcess();
    else if (pid > 0)
        parentProcess();
    else
    {
        printf("fork() failed!\n");
        return 1;
    }

    return 0;
}

void childProcess()
{
    int i = 0;

    for (i = 1; i <= MAX_COUNT; i++)
        printf("This line is from child, value = %d\n", i);
    printf("*** Child process is done ***\n");
}

void parentProcess()
{
    int i = 0;

    for (i = 1; i < MAX_COUNT; i++)
        printf("This line is from parent, value = %d\n", i);
    printf("*** Parent is done ***\n");
}