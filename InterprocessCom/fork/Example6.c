#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    puts("Start of program");
    /* make two processes which run same program
        after this instruction */
    fork();

    printf("Hello world!\n");
    puts("End of program");
    return 0;
}