#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MAX_COUNT 10
#define BUF_SIZE 100

int main()
{
    pid_t pid;
    char buf[BUF_SIZE];

    fork();
    pid = getpid();
    for (int i = 1; i <= MAX_COUNT; i++)
    {
        sprintf(buf, "This line is from pid %d, value = %d\n", pid, i);
        write(1, buf, strlen(buf)); //used instead of printf to print
        // because printf is buffered, 
        // will group the output of the processes together
        // you may not print results in expected order, because
        // as parent, child process send output to the same buffer
        // output would be mixed, so write function is used as unbuffered
        // also it depends on CPU schedule
    }
}