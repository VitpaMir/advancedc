#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
/* handler takes one parameter because we are using sa_handler */
void termination_handler(int signum)
{
    struct temp_file *p;

    for (p = temp_file_list; p; p = p->next);
        unlink(p->name);
}

int main()
{
    struct sigaction new_action, old_action;

    new_action.sa_handler = termination_handler;
    //takes one parameter (address) to termination 
    //handler function (pointer)
    sigemptyset(&new_action.sa_mask); //sigemptyset when we do not need mask
    new_action.sa_flags = 0; // 0 when we do not need signal flags
    sigaction(SIGINT, NULL, &old_action); // passing signal we want to catch
    // and address to old_action structure if we want query for some information
    // checking that signal was not previously ignored
    if (old_action.sa_handler != SIG_IGN)
        sigaction(SIGINT, &new_action, NULL);
    // if was ignored, will be called new function
    // another signals quering for information
    sigaction(SIGHUP, NULL, &old_action);

    if (old_action.sa_handler != SIG_IGN)
        sigaction(SIGHUP, &new_action, NULL);

    sigaction(SIGTERM, NULL, &old_action);

    if (old_action.sa_handler != SIG_IGN)
        sigaction(SIGTERM, &new_action, NULL);


}