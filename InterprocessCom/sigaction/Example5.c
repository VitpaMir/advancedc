/* Using sa_sigaction to additional fields :
        siginfo_t struct and void pointer*/
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
/* Handler takes three parameters because we are using sig_action */
static void hdl(int sig, siginfo_t *siginfo, void *context)
{
    printf("Sending PID: %ld, UID: %ld\n", (long)siginfo->si_pid, (long)siginfo->si_uid);
    /* give information about si_pid i si_uid about signal*/
}

int main(int argc, char *argv[])
{
    struct sigaction act;
    memset(&act, '\0', sizeof(act)); //initialize act sigaction struct to all zeros
    /* Use the sa_sigaction field because the handles has two addtional parameters,
     hdl contains a siginfo_t and a void pointer, see the function prototype */
    act.sa_sigaction = &hdl;
    /* The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field not
     sa_handler. */
    act.sa_flags = SA_SIGINFO;

    if (sigaction(SIGTERM, &act, NULL) < 0)
    {
        perror("sigaction"); //we have an error
        return 1;
    }

    while(1)
        sleep(3); //waiting for terminate every specified time
    return 0;
}