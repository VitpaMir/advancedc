#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

void signalHandler(int signalValue);

int main()
{
    int i;
    int x;

    signal(SIGINT, signalHandler);
    srand(clock());

    for (i = 1; i <= 100; i++)
    {    
        x = 1 + rand() % 50;
        //generate random numbers to raise SIGINT
        // raise sigint when x is 25
        if (x == 25)
            raise(SIGINT);
        printf("%4d", i);
        if (i % 10 == 0)
            printf("\n");
    }
    return 0;
}

void signalHandler(int signalValue)
{
    int response;

    printf("%s%d%s\n%s\n", "\nInterrupt signal (", signalValue, ") received.", 
        " Do you wish to continue? (1 = yes or 2 = no)");
    scanf("%d", &response);

    while (response != 1 && response != 2)
    {
        printf("(1 = yes or 2 = no)?");
        scanf("%d", &response);
    }
    /* determine if it is time to exit */
    if (response == 1)
        signal(SIGINT, signalHandler);
        /* reregister signal handler for next SIGINT */
    else
        exit(EXIT_SUCCESS);
}