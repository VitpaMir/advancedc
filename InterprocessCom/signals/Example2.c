#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

void handler_dividedByZero(int signum);

int main()
{
    int res = 0;
    int v1 = 0, v2 = 0;
    void (*sigHandlerReturn)(int);
    sigHandlerReturn = signal(SIGFPE, handler_dividedByZero);

    if (sigHandlerReturn == SIG_ERR)
    {
        perror("Signal Error: ");
        return 1;
    }

    v1 = 121;
    res = v1/v2;
    printf("Result of division by zero: %d\n", res);

    return 0;
}

void handler_dividedByZero(int signum)
{
    if (signum == SIGFPE)
    {
        printf("Received SIGFPE, Divide by zero Exception\n");
        exit(0);
    }
    else
    {
        printf("Received %d signal\n", signum);
        return;
    }
}