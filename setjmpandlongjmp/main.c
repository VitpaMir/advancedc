#include <stdio.h>
#include <setjmp.h>
#include <stdlib.h>
void error_recovery();
void func1();
void func2();

static jmp_buf jbuf;
static int exception_type;

int main()
{

    while (1)
    {
        if(setjmp(jbuf))
        {
            printf("Back in main.\n");
            break;
        }
        else
            error_recovery();
    }
    return 0;
}

void error_recovery()
{
    printf("Error discovered.\n");
    longjmp(jbuf, 1);
}