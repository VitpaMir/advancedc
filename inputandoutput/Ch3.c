#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#ifdef GETLINE_EXIST
ssize_t getline(char **lineptr, size_t *n, FILE *stream);
#endif

#define MAX_LINE 256
void using_getline(FILE * fp, int ch);


int main(int argc, char * argv[])
{
    FILE * fp;
    char ch;
    char line1[MAX_LINE];

    if (argc == 3)
    {
        ch = argv[1][0];
        if ((fp = fopen(argv[2], "r")) == 0)
        {
            fprintf(stderr, "Unable to open the file.\n");
            exit(0);
        }
        // using getline:
        using_getline(fp, ch);
        rewind(fp);
        printf("\nfgets usage:\n");
        while (!feof(fp))
        {
            fgets(line1, MAX_LINE, fp);
            for (int i = 0; i < strlen(line1); i++)
            {
                if (line1[i] == ch)
                {
                    fputs(line1, stdout);
                    break;
                }
            }
        }
        if (fclose(fp) != 0)
        {
            fprintf(stderr, "Unable to close file properly!\n");
            exit(2);
        }
    }
    else if (argc < 3)
    {
        fprintf(stderr, "Wrong input. Proper input: %s, <character> <filename>.\n", argv[0]);
        exit(1);
    }

    return 0;
}

void using_getline(FILE * fp, int ch)
{
    //char line1[MAX_LINE];
    char *buffer = NULL;
    size_t bufsize = 32;
    buffer = (char *) malloc(bufsize * sizeof(char));
    if (buffer == NULL)
    {
        fprintf(stderr, "Error with allocation.\n");
        exit(3);
    }
    printf("Getline usage:\n");
    puts("Obtained lines:");
    while (!feof(fp))
    {
        getline(&buffer, &bufsize, fp);
        for (int i = 0; i < bufsize / sizeof(char); i++)
        {
            if (buffer[i] == ch)
            {
                fputs(buffer, stdout);
                break;
            }
        }
    }
    free(buffer);
}

#define GETLINE_EXIST
/* The original code is public domain -- Will Hartung 4/9/09 */
/* Modifications, public domain as well, by Antti Haapala, 11/10/17
   - Switched to getc on 5/23/19 */

#include <errno.h>
#include <stdint.h>

// if typedef doesn't exist (msvc, blah)
typedef intptr_t ssize_t;

ssize_t getline(char **lineptr, size_t *n, FILE *stream)
{
    size_t pos;
    int c;

    if (lineptr == NULL || stream == NULL || n == NULL) {
        errno = 22; //EINVAL, but hard to reach in library errno.h I do not know why...
        return -1;
    }

    c = getc(stream);
    if (c == EOF) {
        return -1;
    }

    if (*lineptr == NULL) {
        *lineptr = malloc(128);
        if (*lineptr == NULL) {
            return -1;
        }
        *n = 128;
    }

    pos = 0;
    while(c != EOF) {
        if (pos + 1 >= *n) {
            size_t new_size = *n + (*n >> 2);
            if (new_size < 128) {
                new_size = 128;
            }
            char *new_ptr = realloc(*lineptr, new_size);
            if (new_ptr == NULL) {
                return -1;
            }
            *n = new_size;
            *lineptr = new_ptr;
        }

        ((unsigned char *)(*lineptr))[pos ++] = c;
        if (c == '\n') {
            break;
        }
        c = getc(stream);
    }

    (*lineptr)[pos] = '\0';
    return pos;
}