#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char * argv[])
{
    FILE * fp, * fp2;
    int ch;

    if (argc == 2)
    {
        if ((fp = fopen(argv[1], "r")) == NULL)
        {
            fprintf(stderr, "Unable to open file.\n");
            exit(0);
        }
        if ((fp2 = fopen("changedWiersz.txt", "w")) == NULL)
        {
            fprintf(stderr, "Unable to write new file.\n");
            exit(0);
        }
        else
        {
            while ((ch = fgetc(fp)) != EOF)
            {
                if (isupper(ch))
                    fputc(tolower(ch), fp2);
                else if (islower(ch))
                    fputc(toupper(ch), fp2);
                else
                    fputc(ch, fp2);
            }
        }
        if (fclose(fp) != 0)
        {
            fprintf(stderr, "Unable to close source text file.\n");
            exit(2);
        }
    }
    else if (argc == 1)
    {
        if ((fp2 = fopen("toLower.txt", "w")) == NULL)
        {
            fprintf(stderr, "Unable to write new file.\n");
            exit(0);
        }
        while ((ch = fgetc(stdin)) != EOF)
        {
            if (isupper(ch))
                fputc(tolower(ch), fp2);
            else if (islower(ch))
                fputc(toupper(ch), fp2);
            else
                fputc(ch, fp2);
        }
    }
    else
    {
        fprintf(stderr, "Wrong input. Proper input: %s <filename>", argv[0]);
        exit(1);
    }
    if (fclose(fp2) != 0)
    {
        fprintf(stderr, "Unable to close written files!\n");
        exit(2);
    }
    return 0;
}