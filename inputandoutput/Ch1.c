#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(int argc, char * argv[])
{
    FILE * fp;
    int counter = 0;
    int words = 0;
    int in_word = 0;
    char ch;
    if (argc == 1)
    {
        while ((ch = getchar()) != EOF)
        {
            counter++;
            if (isalnum(ch) && in_word == 0)
            {
                words++;
                //printf("LOG: detected word\n");
                in_word = 1;
            }
            else if ((isspace(ch) || ispunct(ch)) && in_word == 1)
            {
                in_word = 0;
                //printf("LOG: koniec slowa, zmiana flagi\n");
            }
        }
    }
    else if (argc == 2)
    {
        if ((fp = fopen(argv[1], "r")) == NULL)
        {
            fprintf(stderr, "File does not exists or unable to open.\n");
            exit(2);
        }
        while ((ch = getc(fp)) != EOF)
        {
            counter++;
            if (isalnum(ch) && in_word == 0)
            {
                words++;
                //printf("LOG: detected word\n");
                in_word = 1;
            }
            else if ((isspace(ch) || ispunct(ch)) && in_word == 1)
            {
                in_word = 0;
                //printf("LOG: koniec slowa, zmiana flagi\n");
            }
        }
        if (fclose(fp) != 0)
        {
            fprintf(stderr, "Unable to close properly.\n");
            exit(3);
        }
    }
    else
    {
        fprintf(stderr, "Wrong input. Proper input %s <filename>\n", argv[0]);
        exit(1);
    }

    printf("Number of words: %d\n", words);
    printf("Number of characters: %d\n", counter);
    return 0;
}