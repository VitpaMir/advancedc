#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 20
void display_results(signed long long int * tab, int size);

int main()
{
    FILE * fp;
    int i = 0;
    int k = 0;
    int l = 0;
    int temp, n = 0;

    long long int buf1[MAX_SIZE];
    long long int even[MAX_SIZE];
    long long int odd[MAX_SIZE];
    long long int prime[MAX_SIZE];

    if ((fp = fopen("numbers.txt", "r")) == 0)
    {
        fprintf(stderr, "Unable to open the file.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        while (!feof(fp))
        {
            //fgets(buf, 10, fp);
            //sscanf(buf, "%lld %lld %lld %lld %lld", &buf1[i++], &buf1[i++], &buf1[i++], &buf1[i++], &buf1[i++]);
            fscanf(fp, "%lld", &buf1[i]);
            i++;
            //fputs(buf, stdout);
        }
    }
    for (int j = 0; j < sizeof(buf1) / sizeof(long long int); j++)
    {
        if (buf1[j] % 2 == 0)
        {
            even[k] = buf1[j];
            k++;
        }
        else
        {
            odd[l] = buf1[j];
            l++;
        }
    }
    for (int m = 0; m < sizeof(buf1) / sizeof(long long int); m++)
    {
        temp = 0;
        for (int num = 2; num < buf1[m] / 2; num++)
        {
            if ((buf1[m] % num) == 0)
            {
                temp++;
            }
        }
        if (temp == 0)
        {
            prime[n] = buf1[m];
            n++;
        }
    }
    if (fclose(fp) != 0)
    {
        fprintf(stderr, "Unable to close the file!.\n");
        exit(EXIT_FAILURE);
    }
    display_results(buf1, i);
    display_results(even, k);
    display_results(odd, l);
    display_results(prime, n);
    return 0;
}

void display_results(long long int * tab, int size)
{
    for (int i = 0; i < size; i++)
        printf("%d ", tab[i]);
    printf("\n");
}