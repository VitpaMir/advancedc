#include <stdio.h>
#include <setjmp.h>
#include <stdlib.h>

jmp_buf buf;
void myFunction();

int main()
{
    if(setjmp(buf))
        printf("Back in main.\n");
    else
        {
            printf("First time through.\n");
            myFunction();
        }
    return 0;
}

void myFunction()
{
    printf("in my function()\n");
    longjmp(buf, 1);

    /* NOT REACHED */
    printf("NEVER SEE THIS LINE BECAUSE LONGJMP.\n");
}