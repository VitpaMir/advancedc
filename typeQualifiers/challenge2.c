#include <stdio.h>

int suma1(int num);

int main()
{
    printf("%d ", suma1(25));
    printf("%d ", suma1(15));
    printf("%d ", suma1(30));

    return 0;
}

int suma1(int num)
{
    static int sum = 0;
    sum += num;
    
    return sum;
}