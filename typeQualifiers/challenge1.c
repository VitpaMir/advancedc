#include <stdio.h>

void fun1();
static double var2; // a global double variable, only accessible inside this file
float var3; // a global float variable that is accessible with the entire program

int main()
{
    register int var5 = 5; // a register int variable
    for (int var1 = 0; var1 < 9; var1++) // (auto) an int variable with block scope and temporary storage
    {
        printf("%d\n", var1);
        fun1();
    }
    return 0;
}

static void fun1() // a function that is only accessible with this file
{
    static float var3 = 22.5; // a float local variable with permanent storage

    printf("%.2f\n", var3++);
}
