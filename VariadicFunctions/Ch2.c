#include <stdio.h>
int recursive_sum(int n);
int main()
{
    int number;
    puts("Specify number to sum up.");
    scanf("%d", &number);
    printf("SUm: %d\n", recursive_sum(number));
    return 0;
}

int recursive_sum(int n)
{
    static int sum = 0;
    if (n != 0)
        recursive_sum(n - 1);
    sum += n;
    return sum;
}