#include <stdio.h>
#include <malloc.h>

double sumall(int am, double numbers[]);

int main()
{
    double * numbers;
    int i = 0;
    numbers = (double *)malloc(sizeof(double) * 2);

    puts("Program to add any integer numbers.");
    puts("Specify numbers to sum them up.");
    puts("To finish, type q.");

    while(scanf("%lf", &numbers[i]))
    {
        if (i >= 1)
            realloc(numbers, i + 1);
        i++;
    }
    printf("Sum all: %.2lf", sumall(i, numbers));

    free(numbers);
    return 0;
}

double sumall(int am, double numbers[])
{
    double sum = 0;
    for (int i = 0; i < am; i++)
        sum += numbers[i];
    return sum;
}