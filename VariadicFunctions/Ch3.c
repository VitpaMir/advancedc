#include <stdio.h>
int recursive_gcd(int one, int two);

int main()
{
    int one, two;
    puts("Greatest common denominator");
    puts("Specify two numbers:");
    printf("First: ");
    scanf("%d", &one);
    printf("Second: ");
    scanf("%d", &two);

    printf("Greatest common denominator of these two numbers: ");
    printf("%d\n", recursive_gcd(one, two));

    return 0;
}

int recursive_gcd(int one, int two)
{
    if (two != 0)
        recursive_gcd(two, one % two);
    else
        return one;
}