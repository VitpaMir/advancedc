#include <stdio.h>
#include <string.h>
void recursive_string(char str[]);

int main()
{
    recursive_string("koniczyla koniczyna");

    return 0;
}

void recursive_string(char str[])
{
    if (strlen(str) > 1)
        recursive_string((str + 1));
    printf("%c", *str);
}