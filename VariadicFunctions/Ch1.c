#include <stdio.h>
#include <stdarg.h>
#include <malloc.h>

double sumall(int am, ...);

int main()
{
    printf("Sum all: %.2lf", sumall(3, 20.5, 15.2, 22.9, 30.1, 1.1, 125.5));

    return 0;
}

double sumall(int am, ...)
{
    double sum = 0;
    va_list parg;
    va_start(parg, am);

    for (int i = 0; i < am; i++)
        sum += va_arg(parg, double);
    
    va_end(parg);
    return sum;
}