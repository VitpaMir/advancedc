#include <stdio.h>

int main()
{
    int maxrows;
    int i, s;
    int row = 0;
    int space = -1;
    int cnt_space;
    
    printf("Rysowanie choinki z gwiazdek.\n");
    printf("Podaj liczbe rzedow do narysowania: ");
    scanf("%d", &maxrows);

    next:
    cnt_space = 0;
    i = 0;
    s = 0;
    r:
    if (i == maxrows - row)
    {
        star: printf("*");
        s++;
        if (s == 1 && row > 0 && row != maxrows)
        {
            space += 2;
            goto central_spaces;
        }
        else if (s < (2*maxrows + 1) && row == maxrows)
            goto star;
        goto ndr;
    }
    printf(" ");
    i++;
    goto r;
    central_spaces:
    if (s == 1 && cnt_space < space)
    {
        printf(" ");
        cnt_space++;
        goto central_spaces;
    }
    else if (cnt_space == space)
        goto star;
    ndr:printf("\n");
    row++;
    if (row < maxrows + 1)
        goto next;
    else if (row == maxrows)
        goto star;
    return 0;
}