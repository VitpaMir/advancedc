Overview threads:
- programs need to do several things at the same time
- games need to update the graphics on the screen while also
 reading input from a game controller
- chat programs will need to read text from the network and send
 data to the network at the same time
- media players will need to stream video to the display as well
 as watch for input from the user controls
Processes:
- can be both single and multithreaded
- simple process do one thing at a time
- in real world, you cannot do all of the tasks at the same time
 (not by yourself)
    - if someone comes into the shop, you may need to stop
     stoking the shelves to help customer
- if you are in a shop alone, you are the same as a simple process
    - you do one thing after another, but always one thing at the time
    - you can switch between tasks to keep everything going
        - but what if there is a blocking operation?
        - what if you are serving someone at the checkout and the phone
         rings?
- most of programs we have written so far have had a single path of
 execution
    - there has only been one task working inside the program's process
     - the main() function is the single path of execution
      (sequentially, line by line)
Working on tasks at the same time
- so how can your code perform several different tasks at once
    - how about creating a process to handle all these tasks (fork)?
        - the answer is no:
- processes take time to create
    - creating a process for each task is not efficient
- processes cannot share data easily
    - separate process have a complete copy of all data from the parent
        - if the child needs to send back to the parent, then you need 
         use some method of IPC (pipes, shared memory, message queues,
         sockets)
- processes are just plain difficult
    - you need to create a chunk of code to generate processes and that
     can make your programs long and messy
Threads
- you need something to start a separate task quickly, can share all of
 your current data and will not require a lot of code to write 
    ( -> use threads)
- threads are a way to divide a single process into sub-runnable parts
    - a separate path of execution inside a program
    - sometimes called lightweight processes
- a thread can also be scheduled independently of the larger program
 that it is inside of (done by the OS)
    - means that a single program may actually use more than 100% of
     CPU resources on multi-processor machines
- a thread has its own unique id, program counter (PC), a register set
 and a stack space just like a process
- threads share memory across each thread by having the same address
 space (unlike multi-processes)
    - two threads have access to the same set of variables and can alter
     each other's variable values
        - if one thread changes a global variable, all of the other 
         threads will see the change immediately
- threads also share OS resources like open files and signals
    - all of the threads will be able to read and write to the same
     files and talk on the same network sockets
Multi-threading
- threads are popular way to improve an application through prallelism
 (simultaneous running of code)
    - several tasks can execute concurrently (many tasks can run in any
     order and possibly parallel)
- multithreaded program is like a shop with several people working in it
- if one person is running the checkout, another is filling the shelves
 and someone else is waxing the surfboards
    - everybody can work without interruptions
    - if one person answers the phone, it won't stop other people in shop
- in the same way that several people can work in the same shop,
 you can have several threads living inside the same process (program)
    - each thread run independently (a thread of execution)
- multi-threading means you can give each thread a separate task
 and they will all be performed at the same time
    - in a browser, multiple tabs can be different threads
    - MS word uses multiple threads, one thread to format text,
     other thread to process inputs etc.
Advantages
- threads require less overhead than "forking" or spawning a new process
    - the system does not initialize a new system virtual memory space
     and environment
- threads provide efficient communication and data exchange because
 they share the same address space
- threaded application offer potential performance gains and practical
 advantages over non-threaded applications in several ways:
    - the creation of thread is much faster (much less operating
     system overhead)
    - faster context switching
    - faster termination of a thread
[In computer science, overhead is generally considered any combination
 of excess or indirect computation time, memory, bandwidth, or other 
 resources that are required to attain a particular goal.]
Disadvantages
- very easy to overlook the consequences of interactions between
 concurrently executing threads
    - considerable potential for very obscure errors in your code
- providing for thread synchronization is the biggest issue
    - the potential for two or more threads attempting to access the
     same data at the same time
- imagine a program with several threads that may access a variable
 containing salary data
- suppose that two independent threads can modify the value
- if one thread accesses the value to change it and the second thread
 does the same before, the first thread has stored the modified value
    - you will have inconsistent data (sprzeczne, niespójne)
In C:
- one thread by default is run (main - code will only run one 
 instruction at a time)
    - unless you count C11, but these threads are not very portable
     and not widely supported
- threading was traditionally provided by hardware and OS support in
 the past
    - implementation differed substantially from each other making it
     difficult for programmers to develop portable threaded application
- in order to take full advantage of the capabilities provided by threads
 a standardized programming interface was required
- in 1995 POSIX became the standard interface for many system calls in
 UNIX including the threading environment
- we are going to write multi-threaded C programs using POSIX threads
    - also known as pthreads, implementation is available with the
     gcc compiler
        - the key model for programming with threads in nearly every
         language (Java, Python and every high-level)
POSIX thread (pthread) libraries
- the POSIX thread libraries are a standard based thread API for C/C++
    - allows one to spawn a new concurrent process flow
    - can be found on almost any modern POSIX-compliant OS
- it is most effictive on multi-processor or multi-core systems where
 the process flow can be scheduled to run on another processor
    - thus gaining speed through parallel or distributed processing
- a thread is spawned by defining a function and it's argument which
 will be processed in the thread
- the purpose of using the POSIX thread library in your software is to
 execute software faster
- pthread functions are defined in a pthread.h header file and 
 implemented in a thread library
- pthread concepts like: scheduling, thread-specific data, canceling,
 handling signals and reader/writer locks will not be covered in this 
 lectures.
- The time required to create a new thread in an existing process is 
 less than the time required to create a new process
- If one thread opens a file with read privileges then other threads
 in the same process can also read from that file
- Thread synchronization is required because all threads of a process 
 share same address space, global variable and same files
- each thread has its own stack (local variables is not shared)
Creating a thread
pthread API
- the functions that comprise the pthreads API can be grouped into
 three major categories:
1. Thread management
    - routines that work directly on threads - creating, detaching,
     joining etc.
    - also include to set/query thread attributes (joinable,
     scheduling etc.)
2. Synchronization
    - routines that manage read/write locks barriers and deal with
     synchronization
    - mutex functions provide for creating, destroying, locking and
     unlocking mutexes (mutual exclusion)
3. Condition variables
    - routines that address communication between threads that share
     mutuxes
    - base upon programmer specified conditions
Operations that can be performed on threads include:
- thread creation
- termination
- synchronization (joins, blocking)
- scheduling
- data management
- process interaction
The lifecycle of thread, much like a process, begins with creation
- threads are not forked from a parent to create a child
- instead they are simply created with a starting function as the entry
 point
- on POSIX OS, there is a library named pthread.h
    - allows you to create threads and perform many operations on
     threads
    - must include this library when creating and using threads
Three functions that are involved in creation of a thread
    - pthread_create, pthread_exit, pthread_join
- pthread_create function is called to create a new thread and make it 
 executable
    - initially, your main() program comprises a single, default
     thread and all other threads must be explicitly created by the 
      programmer
- the maximum number of threads that may be created by a process is
 implementation dependent
    - once created, threads are peers and may create other threads
    - there is no implied hierarchy or dependency between threads
pthread_create
int pthread_create(pthread_t *thread, const pthread_attr_t *attr, 
                    void *(*start_routine)(void*), void *arg);
- first argument is of type pthread_t
    - an integer used to identify the thread in the system
    - upon succesful completion, pthread_create() stores the ID of
     created thread in the location referenced by thread
- second specifies attributes for the thread,
    - you can specify a thread attributes object or NULL for default
     values
    - examples of attributes that can be specified include detached 
     state, scheduling policy, scope, stack address and stack size 
- third argument is name of the function that thread will execute once
 it is created
- the fourth argument is used to pass arguments to the function
 (start_routine)
    - pointer cast of type void is required
    - NULL may be used if no argument is to be passed
    - to pass multiple arguments, you would need to use pointer to a 
     structure
pthread_join
- is often used to be able to identify when a thread has completed or
 exited
- the method for doing this is to join the thread, which is a lot the
 wait() call for processes
- a join is performed when one wants to wait for a thread to finish
    - used to link the current thread process to another thread
    - a thread calling routine may launch multiple threads then 
     wait for them to finish to get the results
- a call to pthread_join blocks the calling thread until the thread 
 with identifier equal to the first argument terminates
    - makes the program stops in order to wait for the end of the 
     selected thread
- typically, only the main thread calls join, but other threads can also
 join each other
- all threads are automatically join when main thread terminates
- also received the return value of your thread function and stores it
 in a void pointer variable
    - once both threads have finished, your program can exit smoothly
int pthread_join (pthread_t thread, void **value_ptr);
- first argument is the thread id of the thread you want to wait for
- second argument is not NULL, this value is passed to pthread_exit()
 by terminating thread
- returning int which we can use as status:
...
if (pthread_join(&th, NULL) != 0)
    perror("Failed to join thread");
...
pthread_exit
- thread can be terminated in a number of ways
    - by explicitly calling pthread_exit
    - by letting the thread function return
    - by a call to the function exit which will terminate the process
     including any threads
- typically, the pthread_exit() routine is called after a thread has
 completed its work and is no longer required to exist
- if main() finishes before the threads it has created finish and
 exits with pthread_exit(), the other threads will continue to execute 
    - otherwise, they will be automatically terminated when main()
     finishes
- although not explicity required to call pthread_exit() at the end of
 the thread function
    - it is good practice to do so, as you may have the need to return
     some arbitrary data back to the caller via pthread_join()
void pthread_exit(void *value_ptr);
- takes argument which makes the value_ptr available to any succesful
 join with the terminating thread
- sometimes it is desirable for a thread not to terminate (e.g a server
 with a worker thread pool)
Example1.c
    - can be solved by placing the thread code in an infinite loop
     and using condition variables
- we have to link to pthread library if we use Linux
$gcc main.c -lpthread
- if we do not use pthread_exit(), program will not execute new thread,
 because we do not "join" ("join" operation waiting for threads, until 
 they has executed) 
- so you have to do join in main(), because new threads will not run,
 without this, using join make main() thread can wait for new threads
- we can add pthread_join() or pthread_exit()
- we can add both because good practice and program safe 

Passing arguments and returning values
- pthread_create() function permits the programmer to pass one
 argument to the thread start routine
- when passing more than one argument, you can create a structure
which contains all of arguments and then pass a pointer to that
 structure in the pthread_create() function
- all arguments must be cast to (void *)
Example2.c - one argument threads
Example3.c - passing multiple argument by a structure
Example4.c - returning values from thread
int pthread_join(pthread_t thread, void **retval);
- takes a double pointer as second parameter
- it means that threads can pass return value
- much as status int for processes, can be any type value
So we pass in pthread_join() address to variable with casting
(void **) &str for example, we need to also be careful if
 we exit threads after using returned variables.
From this we can notice that processes differs with threads.
Threads can return values as reference to those values.
 Variables used as pointers (addresses to variables) 
 are allocated on the heap. So we need to be careful with 
 exiting threads and it is naturally that threads (main also)
 are sharing resources (memory).

pthread_t is data type used as id of new thread
 (unsigned long)

Common thread functions
- other functions which are useful from pthread library
pthread_self - returns unique system assigned thread id of
 the calling thread.
Example5.c
pthread_detach()
int pthread_detach(pthread_t thread);
- when a thread is created, one of its attributes defines wheater
 it is joinable or detached (oddzielny)
    - by default if you passed NULL as the second argument the 
     thread will be in joinable state
    - only threads that are created as joinable can be joined
        - if thread is created as detached, it can never be joined
- if you know in advance that a thread will never need to join with
 another thread, consider creating it in a detached state
    - some system resources may be able to be freed
- there are two common use cases of when you would want to detach
    - in a cancellation handler for a pthread_join()
        - nearly essential to have a pthread_detach() in order to
         detach the thread on which pthread_join() was waiting
    - in order to detach the "initial thread" (as may be desirable
     in processes that set up server threads)
- the pthread_detach() routine can be used to explicitly detach 
 a thread even though it was created as joinable
Example6.c 
- because in new thread in thread function is sleep() function
 called, first main thread will end running and then new thread
 will end. First was created new thread, printed out message and
 then we detached pthread_self(). And then we printed out thread
 function. Threads are not joined here, we use detached inside
 thread function. Sleep for one second gave time for processor 
 to back to the main thread. 
Stack management functions for threads
- POSIX standard does not dictate the size of a thread's stack
    - implementation dependent and varies 
- exceeding the default stack limit is often very easy to do
    - results in program termination and/or corrupted data
- safe and portable programs do not depend upon the default 
 stack limit
    - instead, explicity allocate enough stack for each thread
     by using the pthread_attr_setstacksize function
pthread_attr_setstacksize(attr, stacksize)
pthread_attr_getstacksize(attr, stacksize)
- you can use it to avoid stack overflow or any kind of stack 
 limit for your thread.
int pthread_attr_getstacksize(const pthread_attr_t *restrict attr,
    size_t *restrict stacksize);
int pthread_attr_setstacksize(pthread_attr_t *attr, size_t stacksize);
Example7.c 
int pthread_attr_init(pthread_attr_t *__attr) - initialize attribute
 variable
In this case we pass in pthread_create() in second argument 
 address to attribute which we set (set new stack size)
pthread_equal
- pthread_equal() compares two thread IDs
    - if the two IDs are different 0 is returned, otherwise a 
     non-zero value is returned
    - operator == should not be used to compare two threads IDs 
     againt each other
int pthread_equal (pthread_t t1, pthread_t t2);
pthread_once
- pthread_once() executes init_routine exactly once in a progress
    - the first call to this function by any thread in the process
     executes the given init_routine, without parameters
    - any subsequent call will have no effect 
int pthread_once(pthread_once_t *once_control, void (*init_routine)());
pthread_once_t - once control variable, determine if init_routine has
been invoked, calling pthread_once() with the same routine, but 
 different once_control variable will result in the routine 
 being called twice, once for each once_control variable.
- the init_routine is typically initialization routine
- the once_control parameter is a synchronization control structure
 that requires initialization prior to calling pthread_once
    - pthread_once_t once_control = PTHREAD_ONCE_INIT;
int pthread_cancel(pthread_t thread);
- cancels particular thread using thread id
- using in thread function passed in pthread_create()

Threads synchronization concepts
- there is some issues with using threads
    - race conditions
    - deadlocks
(all these issues are related to thread synchronization)
- concepts that can prevent these issues
- thread safe code is our goal
- thread synchronization is the concurrent execution of two or more
 threads that share critical resources
- threads should be synchronized to avoid critical resource use
 conflicts
    - otherwise, conflicts may arise when parallel-running threads
     attempt to modify a common variable at the same time
        (result of sharing same address space, you can access it 
         too quickly or in such a way that data is not consistent)
Race conditions (hazard elektroniczny)
- while the code may appear on the screen in the order you wish the
 code to execute
    - threads are scheduled by the operating system and are executed
     at random 
- it cannot be assumed that threads are executed in the order they
 are created 
    - they may also execute at different speeds
- when threads are executing (racing to complete) they may give 
 unexpected results (race conditions)
- race condition often occurs when two or more threads need to perform
 operations on the same memory area 
    - but the results of computations depends on the order in which 
     these operations are performed
- may occur when commands to read and write a large amount of data are
 received at almost the same instant
    - the machine attempts to overwrite some or all of the old data 
     while that old data still being read
- you have things that need to be executed in order, but cannot be
 guaranteed from the operating system, because OS is randomly
 scheduling things, so if things are order dependent, race condition
 is happening
Deadlocks (zakleszczenie, blokada wzajemna)
- can occur when multiple threads are trying to access a shared 
 resource
- deadlock is a situation in which two threads are sharing the same
 resource and effectively preventing each other from accessing this
 resource
    - causes program execution to halt indefinitely
    - each thread is waiting on the other thread
- traffic gridlock is example in real life
- dining philosophers problem is a common example of a deadlock
    - each philosopher picks up his or her left fork and waits 
     for the right fork to become available, but it never does
- deadlocks occur when one thread "locks" a resource and never
 "unlocks" that resource
    - caused by poor application "locks" or joins
    - you have to be very careful when applying two or more 
     "locks" to a section of code
Thread safe code
- because of the issues of race conditions and deadlocks a
 threaded function must call functions which are "thread safe"
    - code only manipulates shared data structures in a manner
     that ensures that all threads behave without uintended 
      interaction
- thread safe means that there are no static or global variables
 (shared resources) which other thread may corrupt or clobber
    - usually any function that does not static data or other 
     shared resources is thread-safe
- thread safe means that program protects shared data
    - possibly through the use of mutual exclusion, atomic 
     operations or condition variables (or locks)
    - you want to strive to write "thread-safe" code
- thread-unsafe functions may be used by only one thread at
 a time in a program and the uniqueness of the thread must be
 ensured
Mutual exclusion (mutex)
- a critical section of code that contains a shared resource
 and is accessible by multiple processes or threads
    - it is important for a thread programmer to minimize 
     critical sections if possible
- mutual exclusion is when a process or a thread is prevented
 simultaneous access to a critical section
    - a property of concurrency control which is used to 
     prevent race conditions
- mutual exclusion is the method of serializing access to
 shared resources
- you do not want to a thread to be modifying a variable that
 is already in the process of being modified by another thread
- another scenario is where a value is in the process of 
 being updated and another thread reads an old value
Mutexes
- is a lock that one can virtually attach to some resource
- one of the primary means of implementating thread 
 synchronization and for protecting shared data 
    - used to prevent data inconsistencies due to race 
     conditions
- anytime a global resource is accessed by more than one 
 thread the resource should have a mutex associated with it
    - can apply a mutex to protect a segment of memory
     ("critical region") from other threads
- if thread wishes to modify or read a value from shared 
 resource, the thread must first gain the lock
    - once it has the lock it may do what it wants with 
     the shared resource without concerns of other threads
     accessing the shared resource because other threads
     will have to wait
    - once the thread finishes using the shared resource
     it unlocks the mutex, which allows other threads to
     access the resource
- like a safe (case) with only one key and resource within it
Atomic operations and condition variables
- atomic operations allow for concurrent algorithms and 
 access to certain shared data types without the use of 
 mutexes
    - one can modify some variable within a multithreaded 
     context without having to go through a locking protocol
- condition variables allow threads to synchronize to a value
 of a shared resource
    - used as a notification system between threads
- you could have a counter (flag) that once it reaches a
 certain count
    - a thread will activate and then wait on a condition
     variable
- active threads will signal on this condition variable to
 notify other threads waiting/sleeping on this condition
 variable
- causing a waiting thread to wake up
- you can also use a broadcast mechanism if you want to 
 signal all threads waiting on the condition variable to
 wake up
- when waiting on condition variables, the wait should be 
 inside a loop

Mutexes
- pthread library provides three synchronization mechanisms
 that we can implement
    - mutexes - locks, block access to variables by other 
     threads
    - joins - makes a thread wait till others are complete
     (terminated)
    - condition variables - a way to communicate to other 
     threads
- joins were provided and examples were shown
    - join is protocol to allow the programmer to collect all 
     relevant threads at a logical synchronization point
      (pthread_join())
Mutexes
- way of synchronizing access to shared resources
- when protecting shared data, it is programmer's responsibility
 to make sure every thread that needs to use a mutex does so
    - if four threads are updating the same data, but only one 
     uses a mutex, the data can still be corrupted
- very often the action performed by a thread owning a mutex 
 is updating of global variables
    - a safe way to ensure that when several threads update the
     same variable, the final value is the same as what if would
     be if only one thread performed the update
- typical sequence in the use of a mutex:
    - create and initialize a mutex variable
    - several threads attempt to lock the mutex
    - only one succeds and that thread owns the mutex
    - the owner thread performs some set of actions 
    - the owner unlocks the mutex
    - another thread acquires the mutex and repeats the process
    - finally the mutex is destroyed
- when several threads compete for a mutex, the losers block at 
 that call
- please understand that a deadlock can occur when 
 using a mutex lock
    - making sure threads acquire locks in an agreed order 
     (i. e. preservation of lock ordering)
Creating and destroying mutexes
- mutex variables must be declared with type pthread_mutex_t
    - they must be initialized/created before they can be used
- there are two ways to initialize/create a mutex variable:
1. Statically, when it is declared
- the below is a mutex variable named lock that is initialized 
 to the default pthread mutex values
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
2. Dynamically, with pthread_mutex_init() function
int pthread_mutex_init(pthread_mutex_t *mutex, 
                        const pthread_mutexattr_t *mutexattr);
- this function requires a pthread_mutex_t variable to operate
 on as the first argument
- attributes for the mutex can be given through the second
 parameter
    - to specify default attributes, pass NULL as the second 
     parameter
- the pthread standard defines three optional mutex attributes
    - protocol - specifies the protocol used to prevent 
     priority inversions for a mutex
    - prioceiling - specifies the priority ceiling of a mutex
    - process-shared - specifies the process sharing of a mutex
- the pthread_mutex_destroy(mutex) function should be used to 
 free a mutex object which is no longer needed
Locking and unlocking mutexes
- to perform mutex locking and unlocking, the pthreads library 
 provides the following functions
int pthread_mutex_lock(pthread_mutex_t *mutex);
    - used by a thread to acquire a lock on the specified mutex
     variable
    - if the mutex is already locked by another thread, this call
     will block the calling thread until the mutex is unlocked
int pthread_mutex_trylock(pthread_mutex_t *mutex);
    - will attempt to lock a mutex, however, if the mutex is 
     already locked, the routine will return immediately with a
     "busy" error code
    - may be useful in preventing deadlock conditions, as in 
     priority-inversion situation
int pthread_mutex_unlock(pthread_mutex_t *mutex);
    - will unlock a mutex if called by the owning thread
    - calling this function is required after a thread has 
     completed its use of protected data if other threads are
     to acquire the mutex for their work with the protected data
    - an error will be returned if:
        - the mutex was already unlocked
        - the mutex is owned by another thread
- anytime a global resource is accesed by more than one thread
 the resource should have a mutex associated with it 
    - the above functions should be used to control access to a 
     "critical section" of code form other threads
    - it is up to the programmer to ensure that the necessary
     threads all make the mutex lock and unlock calls correctly
Example8.c - creating mutexes dynamically
Example9.c - creating mutexes statically
Simple deadlock example:
- the order of applying the mutex is also important
    - potential for deadlock
void *function1()
{
    ...
    pthread_mutex_lock(&lock1);     - execution step 1
    pthread_mutex_lock(&lock2);     - execution step 3 DEADLOCK!!!
    ...
    ...
    pthread_mutex_lock(&lock2);
    pthread_mutex_lock(&lock1);
    ...
}
So this has really not good code here, 
because when you're doing two locks like that, you
could be waiting on here, locked two forever, 
while somebody else is waiting on lock one and you have
to lock one lock.
Example10.c - race condition, deadlock example
- one resource locked by lock1 and second locked by lock2
- in runtime functions thread are locked so resources
 waiting for each other and caused deadlock
- solution for this is using trylock function which does not 
 block, we can put it inside loop (Example11.c)
- in loop we can trylock, if attempt fail, lock1 will be
 unlocked (good practice to make interval between attempts to
 not exaust CPU) and then lock lock1 again.
- if attempt succesful, we acquired resource, we can unlock 
 lock2 and do critical job by this function and unlock lock1
 when job finished in this resource function

Condition variables
- another way for threads to synchronize
- while mutexes implement synchronization by controlling thread 
 access to data, condition variables allow threads to synchronize
 based upon the actual value of data
- without condition variables, the programmer would need to have
 threads continually polling (possibly in critical section),
 to check if a condition is met (ciagle odpytywac)
    - can be very resource consuming since the thread would be 
     continuously busy in this activity
- a condition variable is a way to achieve the same goal 
 without polling
- the condition variable mechanism allows threads to suspend 
 execution and relinquish the processor until some condition
 is true (porzucic)
    - used with the appriopriate functions for waiting and 
     later, thread continuation
- a condition variable must always be associated with a mutex
    - to avoid a deadlock created by one thread preparing to 
     wait and another thread which may signal the condition
     before the first thread actually waits on it
    - the thread will be perpetually waiting for a signal
     that is never sent
- any mutex can be used with a condition variable
    - there is no explicit link between the mutex and the 
     condition variable
Example
- analogy: thread sleeping on pillow, one thread wanted to 
 access data but shared data is not ready yet, so thread 
 decided to sleep on a nearby condition variable until
 another thread updated shared data and woke him up
- another thread updated data, noticed that first asleep, so
 signaled him to wake up off the condition variable
- first thread now play with new shared data
- analogy missed that to access shared data a threads need to
 hold a lock
    - we must have any thread that sleeps on a condition 
     variable release the lock while asleep
    - but since first thread had the lock when he fall asleep,
     he expects to still have lock when he wakes up
    - so the condition variable semantics guarantee that a 
     thread sleeping on a condition variable will not fully 
     wake up until:
     1. it receives a wake-up signal
     2. it can re-acquire the lock that it had when it fell
      asleep
Condition variables functions 
- condition variables must be declared with type pthread_cond_t
 and must be initialized before they can be used
Creating/destroying
 - dynamically created:
int pthread_cond_init(pthread_cond_t *cond, pthread_condattr_t *attr);
- statically:
pthread_cont_t cond = PTHREAD_COND_INITIALIZER;
- destroying:
int pthread_cond_destroy(pthread_cond_t *cond);
Waiting on condition:
- pthread_cond_wait - will put the caller thread to sleep on the 
 condition variable and release the mutex lock, guaranteeing that
 when the subsequent line is executed after the caller has woken up,
 the caller will hold lock
- pthread_cond_timedwait - place limit on how long it will block
int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex);
int pthread_cond_timedwait(pthread_cond_t *cond,pthread_mutex_t *mutex,
                           const struct timespec *abstime);
Waking thread based on condition signal:
int pthread_cond_signal(pthread_cond_t *cond);
pthread_cond_broadcast - wake up all threads blocked by the specified
 condition variable
int pthread_cond_broadcast(pthread_cond_t *cond);
- for all of this functions, the caller must first hold the lock that 
 is associated with that condition variable
    - failure to do this can lead to several problems
Example12.c 
- you need to ensure that when you do the signals, all other threads 
 are in waiting state, because if thread comes in after, you have a 
 deadlock situation