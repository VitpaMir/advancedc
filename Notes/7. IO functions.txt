Char functions (input)
- these functions are contained in the standard C library
stdio.h contains functions declarations and macro definitions associated with
the io routines from the standard library
Char functions:
getc
getchar
fgetc
ungetc
putc
putchar
fputc
String functions:
gets
fgets
puts
fputs
getline
Formatting functions:
sprint
fprintf
fflush
fscanf
sscanf
Originally that io functions were not part of the definitions of C
Standard functions must work in a wide variety of computer environments
When a C program is executed, three files are automatically opened by the system for
use by the program: stdin, stdout, stderr (stdio.h)
- stdin indetifies with standard input of the program and is normally also associated with
terminal window, all standat io functions that perform input and do not take a FILE pointer
as an argument get their input from stdin
- stdout refers to standard output, which is normally also associated with terminal window
stderr identifies the standard error file where most of the error messages produced by
the system are written and is also normally associated with terminal
getc - read single character from file (stdin can also be passed)
int getc(FILE *stream);
getchar - read from stdin
int getchar(void);
int fgetc(FILE * stream);
fgetc cannot be implemented as macro (getc can), is guaranteed to be
a function, we can take its address (e.g to pass as argument to another function)
ungetc - enables you to put character you have just read from input string,
int ungetc(int ch, FILE * stream);
- put back specified characters back to the stream to further processes
For example:
#include <stdio.h>
int main()
{
    char ch = 0;
    while (isspace(ch = (char)getchar()));
    //odczytuje tylko znaki spacji do otrzymania innego
    ungetc(ch, stdin); //pobierze pierwszy znak niebedacy spacja podany w petli powyzej
    printf("char is %c\n", getchar()); // wypisze ostatni znak zwrocony przez ungetc c
    return 0;                           //do strumienia danych wejsciowych
}

Output char functions
putc - writes a single character to a file (or stdout)
- takes two arguments, first is character that is written into a file
- second is the FILE pointer of file where character is written to
- must first call fopen() in write or append mode
int putc(int ch, FILE * fp);
int putchar(int c);//put character on the screen (stdout)
example
#include <stdio.h>
int main()
{
    int ch = 0;
    while ((ch = getchar()) != EOF)
        putchar(ch);
    ungetc(ch, stdin);// ungetc() returns EOF previously read back to stdin
    // if insted of EOF, we use '#', ungetc() will take characters after '#' character,
    // and puts in on screen
    printf("Thank you!\n");
    return 0;
}
int fputc(int character, FILE * stream); //stdout can be used, but mainly to a files
fputc - write input a single character to a file, moves the file pointer position to the 
next location in a file.
This functions work nice with while loop to read or write more character until EOF.
int remove(const char *filename), int rename(const char *old_filename, const char *new_filename) in stdio.h

String functions
gets - get string, read line from standard input, until terminating new line or EOF
char * gets(char *str);
- deprecated, removed from C11 because could overflow stack (stores data from stream
input even if buffer is full)
fgets - used for reading entire lines from file/stream
- similar behaviour to gets()
char * fgets(char * buffer, int n, FILE * stream);
'n' is an integer that represents maximum number of characters to be stored into buffer,
including null character,
- reads from file or stream until a newline character has been read or n - 1 character
- null character is written immediately after the last character read
- returns the value of buffer if successful
- returns NULL if error occurs or if attempt is made to read past the end of the file
- retains newline character
- it is possible to read a partial line
- also deprecated because function cannot tell whether the null character
is included in the string it reads
- only use when data read cannot contain a null character
- otherwise use getline
int feof(FILE * stream)
int ferror(FILE * stream)
getline - C11, reads line of a string of text from stream or file
- more reliable than others, include newline character
ssize_t getline(char **buffer, size_t *size, FILE * stream);
First parameter is a pointer to a block allocated with malloc or calloc (type **char)
- address of the first character position where the input string will be stored
- will automatically enlarge the block of memory as needed (realloc)
- will contain the line read by getline when it returns
Second parameter is pointer to a variable of type size_t
- specifies the size in bytes of the block of memory pointed to by first parameter
- the address of the variable that holds the size of the input buffer, another pointer
Return -1 when error (such as EOF) without reading any bytes
- otherwise returns the number of characters read (without final null character)
Example:
char *buffer = NULL;
size_t buffsize = 32;
size_t characters;
buffer = (char *) malloc(buffsize * sizeof(char));
characters = getline(&buffer, &bufsize, stdin);
printf("%zu" characters were read.\n, characters);
printf("You typed: '%s'", buffer);
return 0;
Puts() - writes line on a screen (string), appends new line
int puts(const char * string);

Formatting string functions
sprintf() - formatted output string function, you can combine several data types
into a character array, in char buffer store(differents types to string conversion)
int sprintf(char * string, const char * format, ...);
First parameter is string where you store data, second const format string with variables
with specifiers and other parameters are variables like in printf()
Returns number of characters stored, without terminating null character.
Need to be sure that buffer size is large enough to avoid buffer overrun.
Unsafe becuase do not check length of the destination buffer.
Leads to security issues, can cause overflow of the buffer (memory).
fprintf() - same as printf() but on a file (takes file pointer)
int fprintf(FILE * stream, const char * format, ...);
fprintf and stderr - can logged error messages
fscanf() - same operation as scanf() but on a file
used to read formatted input from a file, also takes file pointer
int fscanf(FILE * fp, const char * format, ...);
Return the value of arguments that are successfully read and assigned
Return EOF if the end of file is reached before the conversion specs have been processed
fscanf(fp, "%d", &i); - reads next integer value and stores in variable i.
sscanf() - allows you to read formatted data from a string rather than
standard input or keyboard
int sscanf(const char * str, const char * control_string [arg1, arg2, ...]);
example: sscanf(buffer, "%s %d", name, &age);
First argument is a pointer to the string from where we want to read the data.
The rest of the arguments is same as scanf, sscanf() returns number of items read from 
the string and -1 if an error is encountered.
Generally, read from string given type of data and put in variable.
From buffer sscanf() reads name (word to first white character) and age - first integer in a string.
if you use fgets() + sscanf(), you must enter both values on the same line
if you only use fscanf() on stdin, it will read them off different lines if it does not
find the second value on the first line you entered
if you read a line that you are unable to parse with sscanf() after having read it using
fgets() your program can simply discard the line and move on
if you read a line using fscanf(), when it fails to convert fields, it leaves all the input
on the stream - so if you failed to read the input you wanted, you would have to go and read
all the data you want to ignore yourself
You can use fscanf() by itself, however, you may be able to avoid some headaches by using
fgets() + sscanf(),
there is no difference between them if
- input data is well-formed
- two types of error occur - i/o and format
- fscanf() simultaneously handles these two error but offer few recovery options
- fgets() and sscanf() allow logical separation of i/o issues from format ones and thus
better recovery
- only one parsing path
- separating i/o from scanning of a buffer does not produce the desired results, other sscanf()
calls with different formats are available
- no embedded '\0' - rarely occur, but should once, sscanf() will not see it as scanning stops
with its occurance, whereas fscanf() continues.
- you need to check results of all three functions.
int fflush(FILE * fp); - used to clean a file or buffer
- causes any unwritten data in output buffer to be sent to the output file
- cleans the buffer if it has been loaded with any other data already
- buffer is a temporary variable or pointer which loads/points to the data
- if the file is a null pointer, all output buffers are flushed
- effect on input stream is undefined
- you can use it with an update stream (any of the read-write modes), provides
that most recent operation using the stream was not input.