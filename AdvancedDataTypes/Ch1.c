#include <stdio.h>

int suma1(int tab[], int s);

int main()
{
    int size;
    int tab[size];

    printf("Specify a size of array: ");
    scanf("%d", &size);
    printf("Specify array elements: ");
    for(int i = 0; i < size; i++)
        scanf("%d", &tab[i]);
    printf("Sum of elements: %d", suma1(tab, size));
    return 0;
}

int suma1(int tab[], int s)
{
    int suma = 0;

    for (int i = 0; i < s; i++)
        suma += tab[i];
    return suma;
}