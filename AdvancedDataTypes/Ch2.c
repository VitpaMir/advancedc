#include <stdio.h>
#include <malloc.h>
#include <complex.h>

struct myArray
{
    int dSize;
    int array[];
};

int main()
{
    struct myArray * arr = NULL;
    int userSize = 0;
    size_t size = sizeof(struct myArray);

    printf("Specify array size: ");
    scanf("%d", &userSize);
    arr = malloc(size + userSize * sizeof(int));

    arr->dSize = userSize;
    
    printf("Fill the array with some dummy data:\n");
    for (int i = 0; i < arr->dSize; i++)
        scanf("%d", &arr->array[i]);

    printf("Your array finally looks like:\n");
    for (int i = 0; i < arr->dSize; i++)
        printf("%d\n", arr->array[i]);

    free(arr);
    return 0;
}