#include <complex.h>
#include <stdio.h>

#define __STDC_WANT_LIB_EXT1__ 1

int main()
{
#ifdef __STDC_NO_COMPLEX__
	printf("Complex numbers are not supported.\n");
	exit(1);
#else
	printf("Complex numbers are supported.\n");
#endif
}