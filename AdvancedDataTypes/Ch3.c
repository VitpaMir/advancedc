#include <stdio.h>
#include <complex.h>
#include <math.h>

int main()
{
    double complex ax = I * I;
    double complex bx = pow(I, 2);
    double complex cx = exp(I*acos(-1));
    double complex dx = 1.0 - 2.0*I;
    double complex ex = dx*conj(dx);

    printf("%.2f + %.2fi\n", creal(ax), cimag(ax));
    printf("%.2f + %.2fi\n", creal(bx), cimag(bx));
    printf("%.2f + %.2fi\n", creal(cx), cimag(cx));
    printf("%.2f + %.2fi\n", creal(dx), cimag(dx));
    printf("%.2f + %.2fi\n", creal(ex), cimag(ex));

    return 0;
}