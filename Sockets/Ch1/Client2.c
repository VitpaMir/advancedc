#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

int createSocket()
{
    short hSocket = 0;
    printf("Create socket.\n");
    hSocket = socket(AF_INET, SOCK_STREAM, 0);
    return hSocket;
}

int connectServer(int hSocket)
{
    int retVal = 0;
    int serverPort = 12346;
    int addrLen = sizeof(struct sockaddr_in);

    struct sockaddr_in rmte = {0};
    rmte.sin_addr.s_addr = inet_addr("127.0.0.1");
    rmte.sin_family = AF_INET;
    rmte.sin_port = htons(serverPort); /* local port */

    retVal = connect(hSocket, (struct sockaddr *)&rmte, addrLen);
    return retVal;
}

int socketRecv(int hSocket, int *Rsp, short RvcSize)
{
    int shortRetval = -1;
    struct timeval tv;
    tv.tv_sec = 20;
    tv.tv_usec = 0;

    if (setsockopt(hSocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv, sizeof(tv)) < 0)
    {
        printf("Time out\n");
        return -1;
    }

    shortRetval = recv(hSocket, Rsp, RvcSize, 0);
    return shortRetval;
}

int main()
{
    int sock = 0, conn = 0;
    int valtoRec = 0;

    sock = createSocket();
    if (sock == -1)
    {
        perror("Cannot create socket.\n");
        return -1;
    }
    puts("Socket created.");

    if ((conn = connectServer(sock)) < 0)
    {
        perror("Cannot connect.\n");
        return 1;
    }
    puts("Connected with server.");

    if (socketRecv(sock, &valtoRec, sizeof(int)) < 0)
    {
        perror("Cannot receive data.\n");
        return 1;
    }

    printf("Received data: %d\n", valtoRec);

    close(sock);

    return 0;
}