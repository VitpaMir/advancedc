#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

int createSocket()
{
    short hSocket = 0;
    printf("Create the socket\n");
    hSocket = socket(AF_INET, SOCK_STREAM, 0);
    return hSocket;
}

int createBind(short hSocket)
{
    int retVal = -1;
    int clientPort = 12346;
    struct sockaddr_in rmte = {0};
    rmte.sin_family = AF_INET;
    rmte.sin_addr.s_addr = htonl(INADDR_ANY);
    rmte.sin_port = htons(clientPort); /* local port */
    
    retVal = bind(hSocket, (struct sockaddr *)&rmte, sizeof(struct sockaddr));

    return retVal;
}

int main()
{
    int socket_desc = 0, sock1 = 0, sock2 = 0;
    int client1Len = 0, client2Len = 0;
    int msg = 0;
    struct sockaddr_in client1;
    struct sockaddr_in client2;

    socket_desc = createSocket();
    
    if (socket_desc == -1)
    {
        perror("Socket cannot be created.\n");
        return -1;
    }
    puts("Socket created");

    if (createBind(socket_desc) < 0)
    {
        perror("bind failed");
        return 1;
    }
    puts("Bind done");
        
    if (listen(socket_desc, 3) < 0)
    {
        perror("Error with listen.");
        return 1;
    }

    printf("Waiting for data from client1...\n");
    client1Len = sizeof(struct sockaddr_in);
    client2Len = sizeof(struct sockaddr_in);

    sock2 = accept(socket_desc, (struct sockaddr *)&client2, (socklen_t *)&client2Len);
    
    if (sock2 < 0)
    {
        perror("accept client2 failed");
        return 1;
    }

    printf("Connection with client2 accepted.\n");

    sock1 = accept(socket_desc, (struct sockaddr *)&client1, (socklen_t *)&client1Len);
    if (sock1 < 0)
    {
        perror("accept client2 failed");
        return 1;
    }
    printf("Connection with client1 accepted.\n");

    if (recv(sock1, &msg, sizeof(int), 0) < 0)
    {
        puts("Cannot receive data!");
        return 1;
    }
    printf("Received: %d\n", msg);
    msg--;


    if (send(sock2, &msg, sizeof(int), 0) < 0)
    {
        printf("Send failed.");
        return 1;
    }
    printf("Data sent.\n");


    close(sock1);
    close(sock2);
    
    return 0;
}