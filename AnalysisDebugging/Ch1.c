#include <stdio.h>

int main()
{
    int i;
    int j;
    for (i = 0; i < 10; i++)
        printf("hello world %d\n", i);

    return 0;
}
/*  Compilation process:
    $gcc main.c - generates a.exe on Windows, on Linux a.out,
    or: $gcc main.c -o main - generates main.exe or main,
    or: $gcc -c Ch1.c (compilation to object file, machine code)
    and: $gcc -o main.exe Ch1.o (linking object files and getting executable)
    - also changing the object name to main
    $gcc -g main.c -o main.exe - ad debugging information
    $ gcc -Wall test.c -o main.exe (enable all warnings)
    $ gcc -Werror Ch1.c -o main.exe (display warnings as errors)
     */