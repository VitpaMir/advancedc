/* Compile with $gcc -D DEBON -g main.c -o main.exe
                $main.exe 2 1 2 3 
(first number is debug level and rest is various numbers)  */
#include <stdlib.h>
#include <stdio.h>
int Debug = 0;

#ifdef DEBON
#define CHECK(level, fmt, ...) \
  if (Debug >= level) \
    fprintf(stderr, fmt, __VA_ARGS__)
#else
#define CHECK(level, fmt, ...)
#endif

int sum(int x, int y, int z) {
  char c = 2;
  int *a = NULL;
  //CHECK(2, "In sum(): ");
  CHECK(2, "x=%d\n", x);
  CHECK(2, "y=%d\n", y);
  CHECK(2, "z=%d\n", z);
  CHECK(2, "a=%ld\n", (long)a);
  *a = 5;

  CHECK(2, "*a=%d\n", *a);

  return (c + x + y + z + *a) / 3;
}

int main(int argc, char *argv[]) {
  int i, j, k;
  int result;

  CHECK(1, "Number of parameters = %d\n", argc);

  if (argc == 1) {
    printf("Please specify three numbers as parameters.\n");
    exit(1);
  }
  Debug = atoi(argv[1]);
  //CHECK(1, "In main: ");
  i = atoi(argv[2]);
  CHECK(2, "i=%d\n", i);
  j = atoi(argv[3]);
  CHECK(2, "j=%d\n", j);
  k = atoi(argv[4]);
  CHECK(2, "k=%d\n", k);

  result = sum(i,j,12) + sum(j,k,19) + sum(i,k,13);

  printf("%d\n", result);
  return 0;
}
