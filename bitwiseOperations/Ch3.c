#include <stdio.h>
#define MASK_RD 0x1

int bin2dec(int binar);
int numSett(int number, int bit);

int main()
{
    int number, n;

    printf("Enter any number: ");
    scanf("%d", &number);
    printf("Enter nth bit to check and set (0-31): ");
    scanf("%d", &n);

    printf("Number before setting %d bit: %d.", n, number);
    printf("Number after setting %d bit: %d.", n, numSett(number, n));

    return 0;
}

int bin2dec(int binar)
{
    int dec = 0;
    int rem, base = 1;
    int tempBin = binar;
    
    while (tempBin)
    {
        rem = tempBin % 2;
        dec += rem * base;
        tempBin /= 10;
        base *= 2;
    }
    return dec;
}

int numSett(int number, int bit)
{
    printf("\n%d bit is set to %d.\n", bit, (number >> bit) & MASK_RD);
    number |=  1 << bit;
    return number;
}