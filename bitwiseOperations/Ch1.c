#include <stdio.h>

int bin2dec(int binar);
int dec2bin(int decimal);

/* int main()
{
    long long int number;
    long long int number2;

    printf("Specify binary number: ");
    scanf("%lld", &number);
    printf("Decimal number converted: %d\n", bin2dec(number));

    printf("Specify decimal number: ");
    scanf("%lld", &number2);
    printf("Binary from this decimal: %d\n", dec2bin(number2));
    

    return 0;
} */

int bin2dec(int binar)
{
    int dec = 0;
    int rem, base = 1;
    int tempBin = binar;
    
    while (tempBin)
    {
        rem = tempBin % 2;
        dec += rem * base;
        tempBin /= 10;
        base *= 2;
    }
    return dec;
}

int dec2bin(int decimal)
{
    int i = 0;
    //int bin[32] = {0};
    int rem;
    int tempDec = decimal;

    /* while (tempDec > 0)
    {
        rem = tempDec % 2;
        tempDec /= 2;
        bin[i] = rem;
        i++;
    }
    for (int j = i - 1; j >= 0; j--)
        printf("%d", bin[j]); */

    // or, better without array:
    i = 1;
    long long int binaryNum = 0;

    while (tempDec != 0)
    {
        rem = tempDec % 2;
        tempDec /= 2;
        binaryNum += rem * i;
        i *= 10;
    }
    return binaryNum;
}