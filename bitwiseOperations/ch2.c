#include <stdio.h>
extern int dec2bin(int decimal);
int main()
{
    int one;
    int two;

    printf("Specify two numbers, first one: ");
    scanf("%d", &one);
    printf("And second one: ");
    scanf("%d", &two);
    printf("Negation of first: %d\n", dec2bin(~(one)));
    printf("Negation of second: %d\n", dec2bin(~(two)));
    printf("And: %d\n", dec2bin(one & two));
    printf("Or: %d\n", dec2bin(one | two));
    printf("Xor: %d\n", dec2bin(one ^ two));
    printf("Shifting to the left by two: %d\n", dec2bin(one << 2));
    printf("Shifting to the left by two: %d\n", dec2bin(two << 2));
    printf("Shifting to the right by two: %d\n", dec2bin(one >> 2));
    printf("Shifting to the right by two: %d\n", dec2bin(two >> 2));

    return 0;
}