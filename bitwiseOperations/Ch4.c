#include <stdio.h>
#include <stdbool.h>

#define SOLID 0
#define DOTTED 1 
#define DASHED 2

#define BLUE 4
#define GREEN 2
#define RED 1

#define BLACK 0
#define YELLOW (RED | GREEN)
#define MAGENTA (RED | BLUE)
#define CYAN (GREEN | BLUE)
#define WHITE (RED | GREEN | BLUE)

const char * colors[8] = {"black", "red", "green", "yellow", "blue", "magenta", "cyan", "white"};

struct onscreenBox
{
    bool boxStyle           :1;
    unsigned int fillColor  :3;
    unsigned int            :4;
    bool iSborder           :1;
    unsigned int borderColor:3;
    unsigned int lineStyle  :2;
    unsigned int            :4;
};

void show_setts(const struct onscreenBox * mB);

int main()
{
    struct onscreenBox myBox = {true, BLUE, true, BLACK, DASHED};

    printf("Original box settings:\n");
    show_setts(&myBox);
    myBox.boxStyle = false;
    myBox.fillColor = YELLOW;
    myBox.lineStyle = SOLID;
    printf("Changed box settings:\n");
    show_setts(&myBox);

    return 0;
}

void show_setts(const struct onscreenBox * mB)
{
    printf("Box is %s.\n", mB->boxStyle == true ? "opaque" : "transparent");
    printf("Fill color is %s.\n", colors[mB->fillColor]);
    printf("Border %s.\n", mB->iSborder == true ? "shown" : "not shown");
    printf("The border color is %s.\n", colors[mB->borderColor]);
    printf("The border style is ");
    switch (mB->lineStyle)
    {
        case SOLID  : printf("solid.\n"); break;
        case DOTTED : printf("dotted.\n"); break;
        case DASHED : printf("dasheds.\n"); break;
        default     : printf("Unkown type.\n");
    }
}