#include <stdio.h>
#include <dlfcn.h>

int main()
{
    void *handle = NULL;

    int (*removeNonA)(char *) = NULL;
    int (*charFreq1)(char *, char) = NULL;

    char stringTest[] = "nie456456wiem45646456zobaczymy";
    
    handle = dlopen("C:\\Users\\hj61t7\\OneDrive - Aptiv\\Dokumenty\\AdvancedC\\advancedc\\Static libraries and shared obj\\Challenge2\\Lib\\lib_StringFunc.dll", RTLD_LAZY);
    if (!handle)
    {
        fputs(dlerror(), stderr);
        exit(1);
    } 

    dlerror();

    removeNonA = dlsym(handle, "rmvNonAlphaChar");
    checkForError();
    (*removeNonA)(stringTest);
    puts(stringTest);

    charFreq1 = dlsym(handle, "charFreq");
    checkForError();
    printf("charFreq %d\n", (*charFreq1)("Mam mnostwo mizerii. Mmm", 'm'));

    dlclose(handle);
    
    return 0;
}

void checkForError()
{
    char *error = NULL;

    if ((error = dlerror()) != NULL)
    {
        fputs(error, stderr);
        exit(1);
    }
}