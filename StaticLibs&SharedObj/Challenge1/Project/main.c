#include <stdio.h>
#include "StringFuncLib.h"

int main()
{
    char stringTest[] = "nie456456wiem45646456zobaczymy";
    char Test2[] = "Lalala";
    char Test5[] = "New one";
    char Test3[20];
    char Test4[15];
    printf("charFreq %d\n", charFreq("Mam mnostwo mizerii. Mmm", 'm'));
    rmvNonAlphaChar(stringTest);
    puts(stringTest);
    if (strConcat(Test2, " Long long lilong long long"))
        fprintf(stderr, "Function strConcat arise exeception.\n");
    puts(Test2);
    strCopy(Test3, Test5);
    puts(Test3);
    substring(stringTest, 2, 10, Test4);
    puts(Test4);

    return 0;
}