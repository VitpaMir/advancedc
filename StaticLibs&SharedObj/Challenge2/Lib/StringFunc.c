#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <malloc.h>

int charFreq(char *str, char searchChar)
{
    int num = 0;
    for (int i = 0; str[i] != '\0'; i++)
    {
        if (str[i] == searchChar)
            num++;
    }
    return num;
}

int rmvNonAlphaChar(char *source)
{
    char ch;
    int i, j;
    for (i = 0; source[i] != '\0'; i++)
    {
        while(!(source[i] >= 'a' && source[i] <= 'z'))
        {
            for (j = i; source[j] != '\0'; j++)
                source[j] = source[j + 1];
            source[j] = '\0';
        }
        
    }
    return 0;
}

int lengthOfString(char * source)
{
    int length = 0;
    while(*source)
    {
        length++;
        source++;
    }
    return length;
}

int strConcat(char *str1, char *str2)
{
    int i;
    size_t dest_len = strlen(str1);

    for (i = 0; i < strlen(str2); i++)
        str1[dest_len + i] = str2[i];

    return 0;
}

void strCopy(char *destination, char *source)
{
    while(*source)
    {
        *destination = *source;
        destination++;
        source++;
    }

    *destination = '\0';
}

int substring(char *source, int from, int n, char *target)
{
    size_t size = strlen(source);
    int i, j;
    
    if (size <= 0)
    {
        fprintf(stderr, "Wrong input size must be positive.");
        return 1;
    }
    if (n >= (size - 1) || from >= size - 1)
    {
        fprintf(stderr, "Wrong input. Boundaries must be lower than input string.");
        return 1;
    }
    if (from > n)
    {
        fprintf(stderr, "Wrong input. Upper boundary is lower than lower boundary.");
        return 1;
    }
    for (i = (from - 1), j = 0; i < (n - 1); i++, j++)
    {
        target[j] = source[i];
    }
    target[j] = '\0';
    
    return 0;
}
