#ifdef MYLIB_H_INCLUDED
#define MYLIB_H_INCLUDED
/*  charFreq()
    str - string to search
    searchChar - character to look for
    return type - int: count for the number 
    of times that character was found */
int charFreq(char *str, char searchChar);
/*  rmvNonAlphaChar()
    source - source string
    return type - int: 0 on success */
int rmvNonAlphaChar(char *source);
/*  lengthOfString()
    source - source string
    return type - int: length of string */
int lengthOfString(char * source);
/*  strConcat()
    str1 - string to concatenate to (resulting string)
    str2 - second string to copy to 
    return type - int: 0 on success */
int strConcat(char *str1, char *str2);
/*  strCopy()
    source - string to copy from
    destination - second string to copy to 
    return type - int: 0 on success */
int strCopy(char *source, char *destination);
/*  substring()
    source string
    from - starting index from where you want to get substring
    n - number of characters to be copied in substring
    target - target string in which you want to store target string
    return type - int: 0 on success */
int substring(char *source, int from, int n, char *target);
#endif