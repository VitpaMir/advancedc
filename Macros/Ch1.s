	.file	"Ch1.c"
	.text
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC0:
	.ascii "Standard macros presentation\0"
LC1:
	.ascii "Current line: %d\12\0"
LC2:
	.ascii "Ch1.c\0"
LC3:
	.ascii "Current file: %s\12\0"
LC4:
	.ascii "Jan 23 2022\0"
LC5:
	.ascii "Current date: %s\12\0"
LC6:
	.ascii "19:44:54\0"
LC7:
	.ascii "Current time: %s\12\0"
LC8:
	.ascii "Is standard used: \0"
LC9:
	.ascii "Yes\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB13:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$16, %esp
	call	___main
	movl	$LC0, (%esp)
	call	_puts
	movl	$6, 4(%esp)
	movl	$LC1, (%esp)
	call	_printf
	movl	$LC2, 4(%esp)
	movl	$LC3, (%esp)
	call	_printf
	movl	$LC4, 4(%esp)
	movl	$LC5, (%esp)
	call	_printf
	movl	$LC6, 4(%esp)
	movl	$LC7, (%esp)
	call	_printf
	movl	$LC8, (%esp)
	call	_printf
	movl	$LC9, (%esp)
	call	_puts
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE13:
	.ident	"GCC: (i686-posix-dwarf-rev0, Built by MinGW-W64 project) 8.1.0"
	.def	_puts;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
