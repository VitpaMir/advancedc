#include <stdio.h>
#define SUM(a, b) ((a)+(b))

int main()
{   
    int num1, num2;

    puts("Macro sum of the numbers");
    puts("Specify two numbers: ");
    scanf("%d %d", &num1, &num2);
    printf("Sum: %d\n", SUM(num1, num2));

    return 0;
}