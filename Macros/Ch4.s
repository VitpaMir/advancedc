	.file	"Ch4.c"
	.text
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
	.align 4
LC0:
	.ascii "Let's check the letter, is upper or lower case: \0"
LC1:
	.ascii "Letter %c is upper case!\12\0"
LC2:
	.ascii "Letter %c is lower case!\12\0"
LC3:
	.ascii "%c is no alphabetic input!\12\0"
LC4:
	.ascii "Letter %c is alphanumeric!\12\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB13:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$32, %esp
	call	___main
	movl	$LC0, (%esp)
	call	_printf
	call	_getchar
	movb	%al, 23(%esp)
	cmpb	$64, 23(%esp)
	jle	L2
	cmpb	$90, 23(%esp)
	jg	L2
	movl	$1, %eax
	jmp	L3
L2:
	movl	$0, %eax
L3:
	cmpl	$1, %eax
	jne	L4
	movsbl	23(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_printf
	jmp	L5
L4:
	movl	$0, 28(%esp)
L5:
	cmpb	$96, 23(%esp)
	jle	L6
	cmpb	$122, 23(%esp)
	jle	L7
L6:
	movl	$1, %eax
	jmp	L8
L7:
	movl	$0, %eax
L8:
	testl	%eax, %eax
	jne	L9
	movsbl	23(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC2, (%esp)
	call	_printf
	jmp	L10
L9:
	movl	$0, 24(%esp)
L10:
	cmpl	$0, 28(%esp)
	jne	L11
	cmpl	$0, 24(%esp)
	je	L12
L11:
	movl	$1, %eax
	jmp	L13
L12:
	movl	$0, %eax
L13:
	testl	%eax, %eax
	jne	L14
	movsbl	23(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC3, (%esp)
	call	_printf
L14:
	cmpb	$47, 23(%esp)
	jle	L15
	cmpb	$57, 23(%esp)
	jg	L15
	cmpb	$64, 23(%esp)
	jle	L16
	cmpb	$90, 23(%esp)
	jle	L17
L16:
	cmpb	$96, 23(%esp)
	jle	L17
	cmpb	$122, 23(%esp)
	jle	L15
L17:
	movl	$1, %eax
	jmp	L18
L15:
	movl	$0, %eax
L18:
	cmpl	$1, %eax
	jne	L19
	movsbl	23(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC4, (%esp)
	call	_printf
L19:
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE13:
	.ident	"GCC: (i686-posix-dwarf-rev0, Built by MinGW-W64 project) 8.1.0"
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_getchar;	.scl	2;	.type	32;	.endef
