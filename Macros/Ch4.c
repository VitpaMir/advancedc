#include <stdio.h>

#define IS_UPPER(C) (((C) >= 'A' && (C) <= 'Z') ? 1 : 0)
#define IS_LOWER(C) (((C) >= 'a' && (C) <= 'z') ? 0 : 1)
#define IS_DIGIT(C) (((C) >= '0' && (C) <= '9') ? 1 : 0)
#define IS_ALPHANUMERIC(C) ((IS_DIGIT(C) && (IS_UPPER(C) || IS_LOWER(C))) ? 1 : 0)

int main()
{
    char ch;
    int status1, status2;
    printf("Let's check the letter, is upper or lower case: ");
    ch = getchar();
    if (IS_UPPER(ch) == 1)
        printf("Letter %c is upper case!\n", ch);
    else
        status1 = 0;
    if (IS_LOWER(ch) == 0)
        printf("Letter %c is lower case!\n", ch);
    else
        status2 = 0;
    if ((status1 || status2) == 0)
        printf("%c is no alphabetic input!\n", ch);

    if (IS_ALPHANUMERIC(ch) == 1)
        printf("Letter %c is alphanumeric!\n", ch);
    
    return 0;
}