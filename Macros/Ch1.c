#include <stdio.h>

int main()
{
    puts("Standard macros presentation");
    printf("Current line: %d\n", __LINE__);
    printf("Current file: %s\n", __FILE__);
    printf("Current date: %s\n", __DATE__);
    printf("Current time: %s\n", __TIME__);
    printf("Is standard used: ");
    if (__STDC__ == 1)
        puts("Yes");
    else
        puts("No");
    
    return 0;
}