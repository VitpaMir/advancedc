#include <stdio.h>

#define SQUARE(x) ((x)*(x))
#define CUBE(x) SQUARE(x) * (x)

int main()
{
    int num;

    printf("Enter any number to find square and cube: ");
    scanf("%d", &num);
    printf("Square: %d, cube: %d\n", SQUARE(num), CUBE(num));
    
    return 0;
}