#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

void *threadFn(void *arg)
{
    pthread_detach(pthread_self());
    sleep(1);
    printf("Thread Fn\n");
    pthread_exit(NULL);
}

int main()
{
    pthread_t tid;
    int ret = pthread_create(&tid, NULL, threadFn, NULL);

    if (ret != 0)
    {
        perror("Thread creation error.\n");
        exit(1);
    }

    printf("After thread created in main.\n");
    pthread_exit(NULL);

    return 0;
}