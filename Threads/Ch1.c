#include <stdio.h>
#include <pthread.h>

#define NTHREADS 10
int counter = 0;

void *func1(void *ptr)
{
    int *x = (int *) ptr;
    printf("id of this thread is: %lu, counter: %d, number: %d\n", pthread_self(), counter, *x);
    printf("id of this thread is: %lu, counter: %d, number: %d\n", pthread_self(), counter, *x);
    counter++;
    pthread_exit(NULL);
}

int main()
{
    int cnt;
    pthread_t thread_arr[NTHREADS];
    int arr[NTHREADS];

    for (int i = 0, cnt = 0; i < NTHREADS; i++)
    {
        arr[i] = cnt++;
        pthread_create(&thread_arr[i], NULL, func1, (void *) &arr[i]);
    }
    for (int i = 0; i < NTHREADS; i++)
    {
        pthread_join(thread_arr[i], NULL);
        pthread_exit(&thread_arr[i]);
    }
    return 0;
}