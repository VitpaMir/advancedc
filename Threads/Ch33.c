/* Another way of this challenge
 from demonstration from course */
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#define NTHREADS 10

pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t cond2 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_cond = PTHREAD_COND_INITIALIZER;

int counter = 0;
int number_evens_finished = 0;

void *func1(void *ptr)
{
    int *x = (int *) ptr;

    pthread_mutex_lock(&condition_mutex);
    // pthread_mutex_lock(&cond2); optionally condition variable for if else
    if (*x % 2 == 0)
        number_evens_finished++;
    else
        pthread_cond_wait(&condition_cond, &condition_mutex);
    
    //pthread_mutex_unlock(&cond2); //unlock optionally cond. var.
    // in this case mutex lock for condition_mutex would be below

    printf("id of this thread is: %lu, counter: %d, number: %d\n", pthread_self(), counter, *((int *) ptr));
    printf("id of this thread is: %lu, counter: %d, number: %d\n", pthread_self(), counter, *((int *) ptr));
    counter++;

    pthread_mutex_unlock(&condition_mutex);
    pthread_exit(NULL);
}

int main()
{
    pthread_t thread_arr[NTHREADS];
    int arr[NTHREADS];

    for (int i = 0, cnt = 0; i < NTHREADS; i++)
    {
        arr[i] = cnt++;
        pthread_create(&thread_arr[i], NULL, func1, (void *) &arr[i]);
    }
    sleep(1);
    
    while (1)
    {
        if (number_evens_finished == NTHREADS / 2)
        {
            pthread_cond_broadcast(&condition_cond);
            break;
        }
    }

    for (int i = 0; i < NTHREADS; i++)
        pthread_join(thread_arr[i], NULL);
    return 0;
}