#include <stdio.h>
#include <pthread.h>

#define NTHREADS 10

int counter = 0;
pthread_mutex_t lock;

void *func1(void *ptr)
{

    pthread_mutex_lock(&lock);
    printf("id of this thread is: %lu, counter: %d, number: %d\n", pthread_self(), counter, *((int *) ptr));
    printf("id of this thread is: %lu, counter: %d, number: %d\n", pthread_self(), counter, *((int *) ptr));
    counter++;
    pthread_mutex_unlock(&lock);
    pthread_exit(NULL);
}

int main()
{
    int cnt;
    pthread_t thread_arr[NTHREADS];
    int arr[NTHREADS];

    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("Mutex initialization failed.\n");
        return 1;
    }

    for (int i = 0, cnt = 0; i < NTHREADS; i++)
    {
        arr[i] = cnt++;
        pthread_create(&thread_arr[i], NULL, func1, (void *) &arr[i]);
    }
    for (int i = 0; i < NTHREADS; i++)
        pthread_join(thread_arr[i], NULL);
    return 0;
}