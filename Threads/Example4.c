#include <stdio.h>
#include <pthread.h>
#include <string.h>

void *hello_return(void *args)
{
    char * hello = strdup("Hello World!!\n");
    return (void *) hello;
}

int main(int argc, char *argv[])
{
    char * str;
    pthread_t thread;

    pthread_create(&thread, NULL, hello_return, NULL);
    
    // wait until the thread completes, assign return value to str
    // this is the difference, we pass second argument as address
    // to string value (thread will store returning value in str)
    pthread_join(thread, (void **) &str);
    printf("%s", str);

    // pthread_exit(NULL); cannot be before printf because it will
    // clean our data and str variable
    return 0;
}