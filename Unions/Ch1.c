#include <stdio.h>

typedef union
{
    char letterGrade;
    int roundedGrade;
    float exactGrade;
} Student;

union another
{
    int one;
    int two[10];
    double four[4];
};

struct Uczen
{
    int number;
    union
    {
        int numberGrade;
        char letterGrade;
    } grade;
};


int main()
{
    Student var1;
    Student var2;
    Student * uptr;
    
    uptr = &var2;

    struct Uczen nowy = {.grade.letterGrade = 'D', .number = 3};
    
    puts("First student grades:");
    printf("%c, %d, %.2f\n", var1.letterGrade, var1.roundedGrade, var1.exactGrade);

    var1.letterGrade = 'E';
    var1.roundedGrade = 50;
    var1.exactGrade = 49.5;
    puts("First student grade again:");
    printf("%c\n", var1.letterGrade);
    puts("First student grade again:");
    printf("%d\n", var1.roundedGrade);
    puts("First student grade again:");
    printf("%.2f\n", var1.exactGrade);


    var2.letterGrade = 'A';
    puts("Second student grade:");
    printf("%c\n", var2.letterGrade);
    var2.roundedGrade = 94;
    puts("Second student grade:");
    printf("%d\n", var2.roundedGrade);
    var2.exactGrade = 93.59;
    puts("Second student grade:");
    printf("%.2f\n", var2.exactGrade);
    printf("Size of union first student: %zu\n", sizeof(var1));
    printf("Size of union second student: %zu\n", sizeof(var2));
    puts("Second student grade pointer:");
    printf("%.2f\n", uptr->exactGrade);

    printf("Nowy: %d, %c", nowy.number, nowy.grade.letterGrade);

    return 0;
    
}