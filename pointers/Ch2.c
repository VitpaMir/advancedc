#include <stdio.h>
#include <malloc.h>
void allocateMemory(int **ptr);

int main()
{
    int *iPtr = NULL;
    allocateMemory(&iPtr);
    *iPtr = 10;

    printf("%d\n", *iPtr);
    free(iPtr);
    return 0;
}

void allocateMemory(int **ptr)
{
    *ptr = (int*) malloc(sizeof(int));
}