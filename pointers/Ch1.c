#include <stdio.h>

int main()
{
    int i = 10;
    int *p;
    int **pp;
    p = &i;
    pp = &p;
    

    printf("Value of integer: %d, %d, %d\n", i, *p, **pp);
    printf("Address of integer: %p, %p, %p\n", &i, p, *pp);
    printf("Value of single pointer: %p, %p\n", p, *pp);
    printf("Address of single pointer: %p, %p\n", &p, pp);
    printf("Value and address of double pointer: %p, %p\n", *pp, &pp);

    return 0;
}