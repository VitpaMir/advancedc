#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    time_t currTime = time(NULL);
    char *t;
    if (currTime == ((time_t) -1))
    {
        fprintf(stderr, "Failure to get current time.");
        exit(EXIT_FAILURE);
    }
    if ((t = ctime(&currTime)) == NULL)
    {
        fprintf(stderr, "Failure to get current time.");
        exit(EXIT_FAILURE);
    }

    printf("%ld\n", time(0));
    printf("Current time is: %s\n", t);
    exit(EXIT_SUCCESS);
}