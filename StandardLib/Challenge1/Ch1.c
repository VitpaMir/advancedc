#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#define MAX 50

int main()
{

    srand(time(0));
    for (int i = 0; i < MAX; i++)
    {
        printf("%lf ", (rand() % 2) - 0.5);
    }
    return 0;
}