#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 20
void fillarray(double ar[], int n);
void displayarray(double ar[], int n);
int sortkowanko(const void *, const void *);
double randRange(double, double);

int main()
{
    double tab[SIZE];

    fillarray(tab, SIZE);
    puts("Array:");
    displayarray(tab, SIZE);
    qsort(tab, SIZE, sizeof(double), sortkowanko);
    puts("Sorted array:");
    displayarray(tab, SIZE);

    return 0;
}

void fillarray(double ar[], int n)
{
    srand(time(0));
    for (int i = 0; i < n; i++)
        ar[i] = randRange(-3.5, 3.5);
}

void displayarray(double ar[], int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%lf ", ar[i]);
        if (i % 5 == 4)
            printf("\n");
    }
    printf("\n");
}

int sortkowanko(const void * a, const void * b)
{
    return ( *(double*)a - *(double*)b );
}

double randRange(double min, double max)
{
    double f = (double) rand() / RAND_MAX;
    return min + f * (max - min);
}