#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main()
{
    time_t time_ptr;
    time(&time_ptr);
    struct tm *timeTM;
    double seconds = 0;

    if ((timeTM = localtime(&time_ptr)) == NULL)
    {
        fprintf(stderr, "Time cannot be converted.");
        exit(EXIT_FAILURE);
    }
    int seconds1 = (timeTM->tm_mday - 1)*24*60*60 + timeTM->tm_hour*60*60 + timeTM->tm_min*60 + timeTM->tm_sec;
    
    timeTM->tm_hour = 0;
    timeTM->tm_min = 0;
    timeTM->tm_sec = 0;
    timeTM->tm_mday = 1;

    seconds = difftime(time_ptr, mktime(timeTM));
    
   /*  if ((currTime = mktime(timeTM)) == -1);
    {
        fprintf(stderr, "Error: unable to make time using mktime");
        exit(EXIT_FAILURE);
    } */
    printf("%.0lf seconds from beginning of the month.\n", seconds);
    printf("%d seconds from beginning of the month.\n", seconds1);
    return 0;
}